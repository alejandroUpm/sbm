#include "LPC17xx.h"
#include "GPIO_LPC17xx.h"
#include "PIN_LPC17xx.h"

#define PUERTO_LED    1
#define LED_1         18

#define PUERTO_INT     0
#define LINEA_INT      16

int valor = 0;

void EINT3_IRQHandler(void)   // unica rutina para la interrupcion
{
    GPIO_PinWrite(PUERTO_LED,LED_1,valor);
    valor = ~valor;
    LPC_GPIOINT->IO0IntClr = 1 << LINEA_INT;    // elimina el flag  de atencion a la interrupcion 
}                                               // los flags son bits wue dicen si la interrupcion esta pendiente de atencion 

//enciende un led mediante un pulsador ayudado de una interrupcion
int main ()
{
  //si voy a usar un pin de IO o I o O
  GPIO_SetDir(PUERTO_LED,LED_1,GPIO_DIR_OUTPUT);
  //configuracion de la interrupcion 
  PIN_Configure(PUERTO_INT,LINEA_INT,PIN_FUNC_0,PIN_PINMODE_PULLUP,PIN_PINMODE_NORMAL); 
  // se pone un 1 y se desplaza 16 posiciones para atacar al 
  // pin 16 del puerto 0 (se puede con una | tambien)
  LPC_GPIOINT->IO0IntEnF = 1 << LINEA_INT; 
  //  habilitacion de la interrupcion
  NVIC_EnableIRQ(EINT3_IRQn);
  
  while(1);
  
}



