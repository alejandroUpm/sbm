
#include "LPC17xx.h"
#include "GPIO_LPC17xx.h"
#include "PIN_LPC17xx.h"

#define PUERTO_LED    1
#define LED_1         18
#define LED_2         20
#define LED_3         21
#define LED_4         23

#define PUERTO_JS  		0
#define UP     				15		//23				
#define DOWN					24    //17 	esta es la configuracion correcta de pines 	
#define RIGHT     		23		//24	pero la actual es para que sea mas facil de controlar 
#define LEFT			    17		//15	visualmente

uint32_t Shift = 0;
int velocidad = 1;

void EINT3_IRQHandler(void)
{
  if (LPC_GPIOINT->IO0IntStatF & (1 << LEFT))
  {
      Shift = 1;    
  }
	if (LPC_GPIOINT->IO0IntStatF & (1 << RIGHT))
  {
      Shift = 0;    
  }
  if (LPC_GPIOINT->IO0IntStatF & (1 << UP))
  {
     if (velocidad < 5) 
     {
        velocidad++;
     }
		 else
		 {
				velocidad = 5;
		 }
  }  
	if (LPC_GPIOINT->IO0IntStatF & (1 << DOWN))
	{
		if (velocidad > 1)
		{
				velocidad--;
		}
		else
		{
				velocidad = 1;
		}
	}

	SysTick_Config((SystemCoreClock/(100*velocidad)));		//necesario configurar el reloj en la interrupcion, para actualizarlo
	LPC_GPIOINT->IO0IntClr |= 1 << LEFT;
	LPC_GPIOINT->IO0IntClr |= 1 << RIGHT;
	LPC_GPIOINT->IO0IntClr |= 1 << DOWN;
	LPC_GPIOINT->IO0IntClr |= 1 << UP;
}


void SysTick_Handler(void)
{
  static uint32_t ticks;
	
		if (Shift == 0)
		{
			switch(ticks++)
			{
				case 0: GPIO_PinWrite(PUERTO_LED,LED_1,1);break;
				case 5: GPIO_PinWrite(PUERTO_LED,LED_1,0);break;
				case 10: GPIO_PinWrite(PUERTO_LED,LED_2,1);break;
				case 15: GPIO_PinWrite(PUERTO_LED,LED_2,0);break;
				case 20: GPIO_PinWrite(PUERTO_LED,LED_3,1);break;
				case 25: GPIO_PinWrite(PUERTO_LED,LED_3,0);break;
				case 30: GPIO_PinWrite(PUERTO_LED,LED_4,1);break;
				case 35: GPIO_PinWrite(PUERTO_LED,LED_4,0);break;
				default:
					if (ticks > 36)
					{
						ticks = 0;
					}
			}
		}
		else
		{
			switch(ticks++)
			{
				case 0: GPIO_PinWrite(PUERTO_LED,LED_4,1);break;
				case 5: GPIO_PinWrite(PUERTO_LED,LED_4,0);break;
				case 10: GPIO_PinWrite(PUERTO_LED,LED_3,1);break;
				case 15: GPIO_PinWrite(PUERTO_LED,LED_3,0);break;
				case 20: GPIO_PinWrite(PUERTO_LED,LED_2,1);break;
				case 25: GPIO_PinWrite(PUERTO_LED,LED_2,0);break;
				case 30: GPIO_PinWrite(PUERTO_LED,LED_1,1);break;
				case 35: GPIO_PinWrite(PUERTO_LED,LED_1,0);break;
				default:
					if (ticks > 36)
					{
						ticks = 0;
					}
			}
		}
}

int main ()
{
	
  GPIO_SetDir(PUERTO_LED,LED_1,GPIO_DIR_OUTPUT);	// configuracion direccion y tipo de pin para los LED
	GPIO_SetDir(PUERTO_LED,LED_2,GPIO_DIR_OUTPUT);
	GPIO_SetDir(PUERTO_LED,LED_3,GPIO_DIR_OUTPUT);
	GPIO_SetDir(PUERTO_LED,LED_4,GPIO_DIR_OUTPUT);
	
	PIN_Configure(PUERTO_JS,RIGHT,PIN_FUNC_0,PIN_PINMODE_PULLDOWN,PIN_PINMODE_NORMAL);
	PIN_Configure(PUERTO_JS,DOWN,PIN_FUNC_0,PIN_PINMODE_PULLDOWN,PIN_PINMODE_NORMAL);
	PIN_Configure(PUERTO_JS,UP,PIN_FUNC_0,PIN_PINMODE_PULLDOWN,PIN_PINMODE_NORMAL);
	PIN_Configure(PUERTO_JS,LEFT,PIN_FUNC_0,PIN_PINMODE_PULLDOWN,PIN_PINMODE_NORMAL);
	
	LPC_GPIOINT->IO0IntEnF |= 1 << RIGHT; 
	LPC_GPIOINT->IO0IntEnF |= 1 << DOWN; 
	LPC_GPIOINT->IO0IntEnF |= 1 << UP; 
	LPC_GPIOINT->IO0IntEnF |= 1 << LEFT; 
	
	NVIC_EnableIRQ(EINT3_IRQn);
	
  SystemCoreClockUpdate();												// funcion para actualizar el reloj 
  SysTick_Config((SystemCoreClock/100));					//frecuencia de 20 milisegundos cada tick, 
																									// multiplicado por el coeficiente velocidad
																									// manejado por la interrupcion
	while(1);
}










