#include "LPC17xx.h"
#include "GPIO_LPC17xx.h"

#define PORT_LED1   1
#define PIN_LED1    18

uint32_t valor = 0;


void SysTick_Handler (void){
    static uint32_t ticks;

    switch (ticks++)
    {
      case 0: GPIO_PinWrite(PORT_LED1, PIN_LED1, 1); break;
      case 5: GPIO_PinWrite(PORT_LED1, PIN_LED1, 0); break;
      case 9: ticks = 0; break;
      default:
          if  (ticks > 10) 
          {
            ticks = 0;
          }
    }
  }

  int main()
  {
    GPIO_SetDir(PORT_LED1,PIN_LED1,GPIO_DIR_OUTPUT);
    SystemCoreClockUpdate();
    SysTick_Config(SystemCoreClock/1000);
    
    while(1);
  }



