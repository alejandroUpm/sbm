
#include "cmsis_os.h"  
#include "LCD.h"
#include "utils.h"
#include "constants.h"

extern char buffer[BUFFER_MAX];

/*----------------------------------------------------------------------------
 *      Thread 1 'Thread_Name': Sample thread
 *---------------------------------------------------------------------------*/
 
 // THREADS
void t_reloj (void const *argument);                             
osThreadId tid_reloj;                                          
osThreadDef (t_reloj, osPriorityNormal, 1, 0); 

void t_LCD (void const *argument);                             
osThreadId tid_LCD;                                          
osThreadDef (t_LCD, osPriorityNormal, 1, 0); 

void t_minutero(void const *argument);                             
osThreadId tid_minutero;                                          
osThreadDef (t_minutero, osPriorityNormal, 1, 0); 

// VIRTUAL TIMERS
void  timer1_seg_Callback(void const *argument);
osTimerDef(timer1_seg,timer1_seg_Callback);
osTimerId timer_1seg_id;

int Init_Thread (void) {

	timer_1seg_id = osTimerCreate(osTimer(timer1_seg),osTimerPeriodic ,NULL);
	
  tid_reloj = osThreadCreate (osThread(t_reloj), NULL);
	tid_minutero = osThreadCreate (osThread(t_minutero), NULL);
	tid_LCD = osThreadCreate (osThread(t_LCD), NULL);
  if (!tid_reloj | !tid_LCD | !timer_1seg_id | !tid_minutero) return(-1);
  
  return(0);
}

void t_reloj (void const *argument) {
		
	osTimerStart(timer_1seg_id,10);
 
	osThreadYield ();                                        
  
}

void t_LCD (void const *argument){
	int seg = 0;
	int min = 0;
		
	osEvent signals;
	
	LCD_reset();
 	
	while(1){
		signals = osSignalWait(0x01,osWaitForever);
		
		if (signals.value.signals == 0x01){
			
			limpiaBuffer(1);
			imprimeLCD("Tiempo: ",1);
			if(min < 10) escribirNumeroEntero(0,1);
			escribirNumeroEntero(min,1);
			imprimeLCD(":",1);
			if(seg < 10) escribirNumeroEntero(0,1);
			escribirNumeroEntero(seg,1);
			copy_to_lcd(1);
			
			if(min == 59 && seg == 59){
				min = 0;
				seg = 0;
				osSignalSet(tid_minutero,0x02);
			}else if (seg == 59){
				min++;
				seg = 0;
				osSignalSet(tid_minutero,0x03);
			}else{
				seg++;
			}
			osSignalClear(tid_LCD,0x01);
		}
						
		osThreadYield ();                                        
	}
}


void t_minutero (void const *argument){
		
	osEvent signals;
	
	static int horas = 0;
	
	while(1){
		
		limpiaBuffer(0);
		imprimeLCD("Horas: ",0);
		escribirNumeroEntero(horas,0);
		copy_to_lcd(0);
		
		signals = osSignalWait(0x00,osWaitForever);
		
		if (signals.value.signals == 0x02){
				horas++;			
		}	
	}
}
void  timer1_seg_Callback(void const *argument){
		osSignalSet(tid_LCD,0x01);
}
