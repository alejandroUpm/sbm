/*----------------------------------------------------------------------------
 * CMSIS-RTOS 'main' function template
 *---------------------------------------------------------------------------*/

#define osObjectsPublic                     // define objects in main module
#include "osObjects.h"    
#include "utils.h"

extern int Init_Thread(void);

/*
 * main: initialize and start the system
 */
int main (void) {
  osKernelInitialize();                    // initialize CMSIS-RTOS

  // initialize peripherals here
	Initialize_peripherals();
	
	Init_Thread();
	
  // create 'thread' functions that start executing,
  // example: tid_name = osThreadCreate (osThread(name), NULL);

  osKernelStart();                         // start thread execution 
}
