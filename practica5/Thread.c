
#include "cmsis_os.h"                                           // CMSIS RTOS header file
#include "constantes.h"
#include "GPIO_LPC17xx.h"

/*----------------------------------------------------------------------------
 *      Thread 1 'Thread_Name': Sample thread
 *---------------------------------------------------------------------------*/
 
void led1 (void const *argument);                             // thread function
void led3 (void const *argument);
void led4 (void const *argument);
osThreadId tid_led1;                                          // thread id
osThreadId tid_led3;
osThreadId tid_led4;
osThreadDef (led1, osPriorityNormal, 1, 0);                   // thread object
osThreadDef (led3, osPriorityNormal, 1, 0);
osThreadDef (led4, osPriorityNormal, 1, 0);




int Init_Thread (void) {

  tid_led1 = osThreadCreate (osThread(led1), NULL);
  tid_led3 = osThreadCreate (osThread(led3), NULL);
  tid_led4 = osThreadCreate (osThread(led4), NULL);
  if ((!tid_led1) | (!tid_led3) | (!tid_led4 )) return(-1);
  
  return(0);
}

void led1 (void const *argument){
  
	static int contador5Ciclos = 0;
	
  while (1) {
    
    GPIO_PinWrite(PUERTO_LED1,PIN_LED1,ON);
    osDelay(200);
    GPIO_PinWrite(PUERTO_LED1,PIN_LED1,OFF);
    osDelay(800);
		
		contador5Ciclos++;
		
		if (contador5Ciclos == 5) osSignalSet(tid_led3,0x01);		
    
    osThreadYield ();  // suspend thread
  }
}

void led3 (void const *argument){
  
	static int contador20ciclos = 0;
	
	osSignalWait(0x01,osWaitForever);	
  while (1) {
		
		
			
		GPIO_PinWrite(PUERTO_LED3,PIN_LED3,ON);
		osDelay(137);
		GPIO_PinWrite(PUERTO_LED3,PIN_LED3,OFF);
		osDelay(137);
		
		contador20ciclos++;
		
		if(contador20ciclos == 14) osSignalSet(tid_led4,0x01);
		if(contador20ciclos == 19) {
			contador20ciclos = 0;
			osSignalWait(0x01,osWaitForever);
		}			
		
    osThreadYield ();                                           // suspend thread
  }
}

void led4 (void const *argument){  
	
	static int contador30Ciclos = 0;
	osSignalWait(0x01,osWaitForever);

  while (1) {
		

		GPIO_PinWrite(PUERTO_LED4,PIN_LED4,ON);
		osDelay(287);
		GPIO_PinWrite(PUERTO_LED4,PIN_LED4,OFF);
		osDelay(287);
		
		contador30Ciclos++;
		
		if(contador30Ciclos == 24) osSignalSet(tid_led3,0x01);
		if(contador30Ciclos == 29){
			contador30Ciclos = 0;
			osSignalWait(0x01,osWaitForever);
		}
		
    osThreadYield ();                                           // suspend thread
  }
}
