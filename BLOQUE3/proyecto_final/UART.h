#include "stdint.h"
#include "utils.h"
#include "Driver_USART.h"

void USART0_cb(uint32_t event);
void USART3_cb(uint32_t event);

void Init_USART(void);

void sendTeraTerm(char cadena[],int cmd);

void ReceiveTeraTerm(char *cmd, int numBytes);

void sendMP3(char cadena[],char cmd);

void ReceiveMP3(char cmd[],int numBytes);


