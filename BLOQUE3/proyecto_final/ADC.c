#include "LCD.h"
#include "LPC17xx.h"


void init_ADC(void){
	
	// Inicializacion LCD
	init_SPI();
	LCD_reset();
	
	// Inicializacion ADC
	LPC_SC->PCONP |=  (1 << 12);								// Alimentamos el periferico ( tras el reset esta a 0 )
	LPC_ADC->ADCR |= (1 << 21);									// Habilita el ADC ( para apagarlo, primero deshabilitarlo )
	
	//PCLKSEL0 &= ~(11 << 24);									// Suministramos al periferico la se�al del reloj del sistema / 4 (25MHz),
																							// tras el reset, estos bits estan a 00, por lo que no hay que modificar el valor del registro
	LPC_ADC->ADCR |= (1 << 8);									// Escalamos el reloj del ADC, diviendolo entre este valor(+1). Debe ser menor de 13MHz
																							// pero interesa que sea proximo : 25/(1+1) = 12.5MHz
	
	LPC_PINCON->PINSEL3 |= (0x3 << 30);					// Configuramos el pin 1.31 para la funcion AD0.5 
	LPC_ADC->ADCR |= (1 << 5);									// Habilita el pin 5 del ADC para su conversion
//	LPC_ADC->ADCR |= (1 << 16);
}

int volumen(void){
	
	uint32_t registro;
	int valor;
	
	LPC_ADC->ADCR |= (1 << 24);									// Iniciamos la conversion
	while((LPC_ADC->ADGDR & 1<<31)==0);					// Bit "DONE" del registro del canal se pone a 1 cuando se ha terminado la conversion, tras la lectura del dato el bit se pone a 0
	
	registro = (LPC_ADC ->ADGDR >> 4) & 0xFFF;
	valor = registro / (4096/30);
	
	
	return valor;
}

