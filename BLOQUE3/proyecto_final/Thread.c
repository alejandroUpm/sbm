#include "ctype.h"
#include "stdio.h"
#include "cmsis_os.h" 
#include "I2C.h" 
#include "LCD.h"
#include "GPIO_LPC17xx.h"
#include "PIN_LPC17xx.h"
#include "LPC17xx.h"
#include "constants.h"
#include "utils.h"
#include "UART.h"
#include "Driver_USART.h"
#include "ADC.h"
#include "string.h"
#include "PWM.h" 


extern ARM_DRIVER_USART  *USARTdrv0;


//variable global de estados
int static estado = MODO_REPOSO;

int static programa_min_seg_hora = PROGRAMA_SEGUNDO;

static int seg = 0;
static int min = 0;
static int horas = 0;

static int segundos = 0;
static int minutos = 0;

static int seg_aux = 0;
static int min_aux = 0;
static int horas_aux = 0;

static char bufferRx[RX_BYTES];
static char bufferTx[8]={0x7e, 0xff, 0x06, 0x0f, 0x00, 0x01, 0x01, 0xef};
static char Vol[8]={0x7e, 0xff, 0x06, 0x06, 0x00, 0x00, 0x00, 0xef};

static char estado_reproduccion = STANDBY;

static int puls_larga = 0;

// DECLARACION DE MUTEX
osMutexDef(uart_mutex);
osMutexId(id_uart_mutex);

osMutexDef(RX_mutex);
osMutexId(id_RX_mutex);

osMutexDef(TX_mutex);
osMutexId(id_TX_mutex);

osMutexDef(LCD1_mutex);
osMutexId(id_LCD1_mutex);

osMutexDef(LCD2_mutex);
osMutexId(id_LCD2_mutex);



// THREADS
void LCD1 (void const *argument);                            
osThreadId tid_LCD1;                                          
osThreadDef (LCD1, osPriorityNormal, 1, 0);

void LCD2 (void const *argument);                            
osThreadId tid_LCD2;                                          
osThreadDef (LCD2, osPriorityNormal, 1, 0);

void reloj (void const *argument);                            
osThreadId tid_reloj;                                          
osThreadDef (reloj, osPriorityNormal, 1, 0);

void estados (void const *argument);
osThreadId tid_estados;
osThreadDef (estados, osPriorityNormal, 1, 0);

void RGB(void const *argument);
osThreadId tid_RGB;
osThreadDef (RGB, osPriorityNormal, 1, 0);

void JS (void const *argument);
osThreadId tid_JS;
osThreadDef (JS, osPriorityNormal, 1, 0);

void uartRX(void const *argument);
osThreadId tid_RX;
osThreadDef(uartRX,osPriorityNormal, 1, 0);

void uartTX(void const *argument);
osThreadId tid_TX;
osThreadDef(uartTX,osPriorityNormal, 1, 0);



//TIMERS
void tid_timer_seg_cb(void const *argument);
osTimerId tid_timer_seg;
osTimerDef(timer_seg,tid_timer_seg_cb);

void timer_pulsacion_larga_cb(void const *argument);
osTimerId tid_pulsacion_larga;
osTimerDef(pulsacion_larga,timer_pulsacion_larga_cb);


int Init_Thread (void) {
  //creacion mutexs
  id_uart_mutex = osMutexCreate(osMutex(uart_mutex)); 
	id_RX_mutex = osMutexCreate(osMutex(RX_mutex));
	id_TX_mutex = osMutexCreate(osMutex(TX_mutex));
	id_LCD1_mutex = osMutexCreate(osMutex(LCD1_mutex));
	id_LCD2_mutex = osMutexCreate(osMutex(LCD2_mutex));
	
	// creacion timers
	tid_timer_seg = osTimerCreate(osTimer(timer_seg),osTimerPeriodic,NULL);
	tid_pulsacion_larga = osTimerCreate(osTimer(pulsacion_larga),osTimerOnce,NULL);
	
	// creacion threads
  tid_LCD1 = osThreadCreate (osThread(LCD1), NULL);
	tid_LCD2 = osThreadCreate (osThread(LCD2), NULL);
	tid_reloj = osThreadCreate (osThread(reloj), NULL);
  tid_estados = osThreadCreate(osThread(estados),NULL);
	tid_RGB = osThreadCreate(osThread(RGB),NULL);
	tid_JS = osThreadCreate(osThread(JS),NULL);
	tid_RX = osThreadCreate(osThread(uartRX),NULL);
	tid_TX = osThreadCreate(osThread(uartTX),NULL);
  
	
  if (!tid_timer_seg | !tid_LCD1 | !tid_LCD2 | !tid_reloj | !tid_estados
			| !tid_pulsacion_larga | !tid_RGB | !tid_TX | !tid_RX 
			| !tid_JS) 
	return(-1);
  
  return(0);
}


void LCD1(void const *argument) {
	
	osEvent signals;
		
	while(1){

			if (estado == MODO_REPOSO){
				
				signals = osSignalWait(0x00,osWaitForever);
				
				if (signals.value.signals == SEGUNDO){
					reset_RGB();
					limpiaBuffer(0);
					imprimeLCD("MR  SBM T: ",0);
					escribirNumeroDecimal(read_temp(),0);
					imprimeLCD(" C",0);
					copy_to_lcd(0);	

					osSignalSet(tid_LCD2,SEGUNDO);
				
				}
			}	
				
		if (estado == MODO_REPRODUCCION){

      osDelay(200);
			osSignalSet(tid_RGB,REPRODUCIENDO);
			limpiaBuffer(0);
      osMutexWait(id_LCD1_mutex,osWaitForever);
			imprimeLCD("  F:",0);
			escribirNumeroEntero(bufferTx[5],0);
			imprimeLCD(" C:",0);
			escribirNumeroEntero(bufferTx[6],0);
			imprimeLCD("   Vol: ",0);
      escribirNumeroEntero(volumen(),0);
      Vol[6] = volumen();
      sendMP3(Vol,8);
      osMutexRelease(id_LCD1_mutex);
			copy_to_lcd(0);
		}
		
		if (estado == MODO_PROGRAMA_HORA){
			signals = osSignalWait(0x00,osWaitForever);
				
			if (signals.value.signals == SEGUNDO){

				limpiaBuffer(0);
				imprimeLCD("MPH SBM T: ",0);
				escribirNumeroDecimal(read_temp(),0);
				imprimeLCD(" C",0);
				copy_to_lcd(0);			  	
			}
		}
	}
}

void LCD2(void const *argument) {
	
	osEvent signals;
//	static int segundos = 0;
//	static int minutos = 0;
	
	while(1){
		
		if (estado == MODO_REPOSO){
		
			signals = osSignalWait(0x00,osWaitForever);
				
			if (signals.value.signals == SEGUNDO){

				osDelay(100);
				limpiaBuffer(1);
				imprimeLCD("    ",1);
				if(horas < 10) escribirNumeroEntero(0,1);
				escribirNumeroEntero(horas,1);
				imprimeLCD(":",1);
				if(min < 10) escribirNumeroEntero(0,1);
				escribirNumeroEntero(min,1);
				imprimeLCD(":",1);
				if(seg < 10) escribirNumeroEntero(0,1);
				escribirNumeroEntero(seg,1);
				copy_to_lcd(1);
				
			}
		}	
		
			
		if (estado == MODO_REPRODUCCION){
			
			signals = osSignalWait(0x00,osWaitForever);

			if(signals.value.signals == SEGUNDO){
				
				osDelay(100);
				limpiaBuffer(1);
				imprimeLCD("      ",1);
				if(minutos<10) escribirNumeroEntero(0,1);
				osMutexWait(id_LCD2_mutex,osWaitForever);
				escribirNumeroEntero(minutos,1);
				imprimeLCD(":",1);
				if(segundos<10) escribirNumeroEntero(0,1);
				escribirNumeroEntero(segundos,1);
				osMutexRelease(id_LCD2_mutex);
				copy_to_lcd(1);	
								
			}							
		}
		
		if (estado == MODO_PROGRAMA_HORA){
			
			osDelay(100);
			limpiaBuffer(1);
			imprimeLCD("    ",1);
			if(horas_aux < 10) escribirNumeroEntero(0,1);
			escribirNumeroEntero(horas_aux,1);
			imprimeLCD(":",1);
			if(min_aux < 10) escribirNumeroEntero(0,1);
			escribirNumeroEntero(min_aux,1);
			imprimeLCD(":",1);
			if(seg_aux < 10) escribirNumeroEntero(0,1);
			escribirNumeroEntero(seg_aux,1);
			copy_to_lcd(1);

		}
		
		osThreadYield ();
	}
}

void reloj(void const *argument) {
	
	osTimerStart(tid_timer_seg,1000);
	
	osEvent signals;
	
	while(1){		

		signals = osSignalWait(0x00,osWaitForever);
		if (estado != MODO_PROGRAMA_HORA){
			if (signals.value.signals == SEGUNDO){
				
				if(seg == 59 && min == 59 && horas == 23 ){
					seg = 0;
					min = 0;
					horas = 0;
				}else if(seg == 59 && min == 59){
					horas++;
					seg = 0;
					min = 0;
				}else if(seg == 59){
					min++;
					seg = 0;
				}else{
					seg++;
				}
			}
		}
		if(signals.value.signals == SEGUNDO){
			if(estado_reproduccion == PLAY){
				segundos++;
				if(segundos == 60){
					segundos = 0;
					minutos++;
				}
			}else if(estado_reproduccion == TARJETA_OUT){
				segundos = 0;
				minutos = 0;
			}
		}
	}
}

void JS (void const *argument) {
	
	osEvent signals;
	
	while(1){
		
		signals = osSignalWait(0x00,osWaitForever);
		
		if(signals.value.signals == PULSACION_CENTER_CORTA){
			pitido();
      if (puls_larga == 0){
        if (estado == MODO_REPRODUCCION && estado_reproduccion != TARJETA_OUT){
          osSignalSet(tid_TX,PULSACION_CENTER);
        }else if (estado == MODO_PROGRAMA_HORA){				
            min = min_aux;
            horas = horas_aux;
            seg = seg_aux;
        }
      }
      puls_larga = 0;
    }
		
		if(signals.value.signals == PULSACION_UP){
      pitido();
			if (estado == MODO_REPRODUCCION && estado_reproduccion != TARJETA_OUT){
				osSignalSet(tid_TX,PULSACION_UP);
				segundos = 0;
				minutos = 0;
			}else if (estado == MODO_PROGRAMA_HORA){				
				if(programa_min_seg_hora == PROGRAMA_SEGUNDO){
          seg_aux = seg_aux + 5;
					if (seg_aux > 59) seg_aux = 0;
        }else if (programa_min_seg_hora == PROGRAMA_MINUTO){
					min_aux++;
					if (min_aux > 59) min_aux = 0;
				}else if (programa_min_seg_hora == PROGRAMA_HORA){
					horas_aux++;
					if(horas_aux > 23) horas_aux = 0;
				}
			}
		}
		
		if(signals.value.signals == PULSACION_DOWN){
      pitido();
			if (estado == MODO_REPRODUCCION && estado_reproduccion != TARJETA_OUT){
				osSignalSet(tid_TX,PULSACION_DOWN);
				segundos = 0;
				minutos = 0;
			}else if (estado == MODO_PROGRAMA_HORA){				
				if(programa_min_seg_hora == PROGRAMA_SEGUNDO){
          seg_aux = seg_aux - 5;
          if (seg_aux < 0) seg_aux = 59;
        }else if (programa_min_seg_hora == PROGRAMA_MINUTO){
					min_aux--;
					if (min_aux < 0) min_aux = 59;	
				}else if (programa_min_seg_hora == PROGRAMA_HORA){
					horas_aux--;
					if(horas_aux < 0) horas_aux = 23;
				}
			}
		}
		
		if(signals.value.signals == PULSACION_RIGHT){
      pitido();
			if (estado == MODO_REPRODUCCION && estado_reproduccion != TARJETA_OUT){
				osSignalSet(tid_TX,PULSACION_RIGHT);
				segundos = 0;
				minutos = 0;
			}else if (estado == MODO_PROGRAMA_HORA){				
        if(programa_min_seg_hora == PROGRAMA_SEGUNDO){
          programa_min_seg_hora = PROGRAMA_HORA;
        }else if (programa_min_seg_hora == PROGRAMA_MINUTO){
					programa_min_seg_hora = PROGRAMA_SEGUNDO;
				}else if (programa_min_seg_hora == PROGRAMA_HORA){
					programa_min_seg_hora = PROGRAMA_MINUTO;
				}
			}
		}
		
		if(signals.value.signals == PULSACION_LEFT){
      pitido();
			if (estado == MODO_REPRODUCCION && estado_reproduccion != TARJETA_OUT){
        osSignalSet(tid_TX,PULSACION_LEFT);
				segundos = 0;
				minutos = 0;
			}else if (estado == MODO_PROGRAMA_HORA){				
				if(programa_min_seg_hora == PROGRAMA_SEGUNDO){
          programa_min_seg_hora = PROGRAMA_MINUTO;
        }else if (programa_min_seg_hora == PROGRAMA_MINUTO){
					programa_min_seg_hora = PROGRAMA_HORA;
				}else if (programa_min_seg_hora == PROGRAMA_HORA){
					programa_min_seg_hora = PROGRAMA_SEGUNDO;
				}
			}
		}
		osThreadYield ();
	}
}


void estados (void const *argument) {
  
	osEvent signals;
		
  while(1){
		
		signals = osSignalWait(0x00,osWaitForever);
		
		if(signals.value.signals == PULSACION_CENTER){
      osTimerStart(tid_pulsacion_larga,1000);
    }
		
		if(signals.value.signals == PULSACION_LARGA){
			if (estado == MODO_REPOSO){
				estado = MODO_REPRODUCCION;
			}else if (estado == MODO_REPRODUCCION){
				estado = MODO_PROGRAMA_HORA;
			}else if (estado == MODO_PROGRAMA_HORA){				
				estado = MODO_REPOSO;
			}
		}		
											
    osThreadYield ();
  }
}


void RGB(void const *argument){
	
	static char OnOff = 0;
	
	while(1){
		if( estado_reproduccion == TARJETA_OUT){
				GPIO_PinWrite(PUERTO_RGB,GREEN,1);
				GPIO_PinWrite(PUERTO_RGB,RED,OnOff);
				GPIO_PinWrite(PUERTO_RGB,BLUE,1);
				OnOff ^= 0x01;
				osDelay(200);
		}else if(estado == MODO_REPRODUCCION && estado_reproduccion == PLAY){
				GPIO_PinWrite(PUERTO_RGB,GREEN,OnOff);
				GPIO_PinWrite(PUERTO_RGB,RED,1);
				GPIO_PinWrite(PUERTO_RGB,BLUE,1);
				OnOff ^= 0x01;
				osDelay(125);
		}else if (estado == MODO_REPRODUCCION && estado_reproduccion == PAUSE){
				GPIO_PinWrite(PUERTO_RGB,GREEN,1);
				GPIO_PinWrite(PUERTO_RGB,RED,1);
				GPIO_PinWrite(PUERTO_RGB,BLUE,OnOff);
				OnOff ^= 0x01;
				osDelay(1000);
		}else{
				reset_RGB();
		}
		osThreadYield ();
	}
}
  

void uartTX(void const *argument){
	
	osEvent signals;
	
	while(1){
    
		signals = osSignalWait(0X00,osWaitForever);
	
		if(signals.value.signals == PULSACION_CENTER){
      pitido();
			if (estado_reproduccion == PAUSE){
				bufferTx[3] = PLAY;
				osDelay(100);
				sendMP3(bufferTx,8);
				osDelay(100);
				estado_reproduccion = PLAY;
				osSignalSet(tid_LCD2,REPRODUCIENDO);
			}else if (estado_reproduccion == PLAY){
				bufferTx[3] = PAUSE;
				osDelay(100);
				sendMP3(bufferTx,8);
				osDelay(100);
				estado_reproduccion = PAUSE;
				osSignalSet(tid_LCD2,REPRODUCE_PAUSA);
			}else if(estado_reproduccion == STANDBY){
				bufferTx[3] = PLAY;
				osDelay(100);
				sendMP3(bufferTx,8);
				osDelay(150);
				estado_reproduccion = PLAY;				
			}
		}
		else if (signals.value.signals == PULSACION_LEFT){
			if(estado_reproduccion == PLAY || estado_reproduccion == PAUSE || estado_reproduccion == STANDBY){
				if (bufferTx[6] > MIN_CANCIONES) bufferTx[6] -= 0x01;
				bufferTx[3] = NEXT_FOLDER;
				osDelay(100);
				sendMP3(bufferTx,8);
				osDelay(100);
				estado_reproduccion = PLAY;
				osSignalSet(tid_LCD2,0x001000);
			}
    }
		else if (signals.value.signals == PULSACION_RIGHT){
			if(estado_reproduccion == PLAY || estado_reproduccion == PAUSE || estado_reproduccion == STANDBY){
				if (bufferTx[6] < MAX_CANCIONES) bufferTx[6] += 0x01;
				bufferTx[3] = NEXT_FOLDER;
				osDelay(100);
				sendMP3(bufferTx,8);
				osDelay(100);
				estado_reproduccion = PLAY;
				osSignalSet(tid_LCD2,0x001000);
			}
    }
		else if (signals.value.signals == PULSACION_UP){
			if(estado_reproduccion == PLAY || estado_reproduccion == PAUSE || estado_reproduccion == STANDBY){
				bufferTx[3] = NEXT_FOLDER;
				if (bufferTx[5] < 3) bufferTx[5] += 0x01;
				bufferTx[6] = 0x01;
				osDelay(100);
				sendMP3(bufferTx,8);
				osDelay(100);
				estado_reproduccion = PLAY;			
				osSignalSet(tid_LCD2,0x001000);
			}
    }
		else if (signals.value.signals == PULSACION_DOWN){
			if(estado_reproduccion == PLAY || estado_reproduccion == PAUSE || estado_reproduccion == STANDBY){
				bufferTx[3] = NEXT_FOLDER;
				if (bufferTx[5] > 1) bufferTx[5] -= 0x01;
				bufferTx[6] = 0x01;
				osDelay(100);
				sendMP3(bufferTx,8);
				osDelay(100);
				estado_reproduccion = PLAY;
				osSignalSet(tid_LCD2,0x001000);		
			}
    }	
		else if (signals.value.signals == SIN_TARJETA){
			estado_reproduccion = TARJETA_OUT;
      pitidoTarjetaOut();
			osMutexWait(id_TX_mutex,osWaitForever);
			bufferTx[3] = SLEEP;
			osDelay(100);
			sendMP3(bufferTx,8);
			osDelay(150);
		}
		else if(signals.value.signals == CON_TARJETA){
			bufferTx[3] = WAKE_UP;
      osMutexWait(id_TX_mutex,osWaitForever);
      osDelay(100);
      sendMP3(bufferTx,8);
      osDelay(150);
      osMutexRelease(id_TX_mutex);
			bufferTx[3] = STANDBY;    // seleccion de la  tarjeta  SD insertada
			osMutexWait(id_TX_mutex,osWaitForever);
      osDelay(150);
			sendMP3(bufferTx,8);
			osDelay(400);
			estado_reproduccion = STANDBY;
		}	
//		osMutexWait(id_TX_mutex,osWaitForever);
//		sprintf(buffer_aux1," TX %d:%d:%d --> ",horas,min,seg);
//		convertToStringTX(bufferTx,buffer_aux2);
//		strcat(buffer_aux1,buffer_aux2);
//		osDelay(150);
//		sendTeraTerm(buffer_aux1,47);
//		osDelay(300);
	}
}

void uartRX(void const *argument){
	
	char buffer_aux1[MAX_BYTES];
	char buffer_aux2[MAX_BYTES];
	
	while(1){
		osMutexWait(id_RX_mutex,osWaitForever);
		ReceiveMP3(bufferRx,10);
    osSignalWait(ENVIADO,osWaitForever);
		osMutexRelease(id_RX_mutex);
		if(bufferRx[3]==TARJETA_OUT){
			estado_reproduccion = TARJETA_OUT;
			estado = MODO_REPOSO;
			osSignalSet(tid_TX,SIN_TARJETA);
		}	
		if(bufferRx[3]==TARJETA_IN){
			estado = MODO_REPRODUCCION;
			osSignalSet(tid_TX,CON_TARJETA);
		}			
		if(bufferRx[3]==FIN_CANCION){
			estado_reproduccion = STANDBY ;
			osMutexWait(id_RX_mutex,osWaitForever);
			segundos = 0;
			minutos = 0;
			osMutexRelease(id_RX_mutex);
		}
		osMutexWait(id_RX_mutex,osWaitForever);
		sprintf(buffer_aux1," RX %d:%d:%d --> ",horas,min,seg);
		convertToStringRX(bufferRx,buffer_aux2);
		strcat(buffer_aux1,buffer_aux2);
		sendTeraTerm(buffer_aux1,47);
	}
}
	

// CALLBACKS DE LOS TIMERS
void tid_timer_seg_cb(void const *argument){
	osSignalSet(tid_reloj,SEGUNDO);
	osSignalSet(tid_LCD1,SEGUNDO);
	osSignalSet(tid_LCD2,SEGUNDO);
}


void timer_pulsacion_larga_cb(void const *argument){
  if(GPIO_PinRead(PUERTO_JS,CENTER)==1){
    osSignalSet(tid_estados,PULSACION_LARGA);
    puls_larga = 1;
  }
}

// CALLBACKS DE LOS USART
void USART0_cb(uint32_t event){
  
	if(event & ARM_USART_EVENT_SEND_COMPLETE){
		osMutexRelease(id_RX_mutex);
	}	
}


void USART3_cb(uint32_t event){
	
//	diferent flags
//	ARM_USART_EVENT_RECEIVE_COMPLETE		 	
//	ARM_USART_EVENT_TRANSFER_COMPLETE   	
//	ARM_USART_EVENT_SEND_COMPLETE			 		
//	ARM_USART_EVENT_TX_COMPLETE						
	if(event & ARM_USART_EVENT_RECEIVE_COMPLETE){
		osSignalSet(tid_RX,ENVIADO);
	}
	
	if(event & ARM_USART_EVENT_SEND_COMPLETE){
		osMutexRelease(id_TX_mutex);
	}
}

