#include "LPC17xx.h"
#include "GPIO_LPC17xx.H"
#include "PIN_LPC17xx.h"
#include "cmsis_os.h" 
#include "PWM.h"

#define PUERTO_ALTAVOZ				2
#define PIN_ALTAVOZ						0

void init_PWM(void){

	//LPC_SC->PCONP |= ( 1 << 6 );              // Inicializamos el periferico. Tras el reset esta habilitado.
	LPC_SC->PCLKSEL0 |= ( 1 << 12 );            // Se�al de reloj de sistema ( 100MHz )
	LPC_PINCON->PINSEL4 |= ( 1 );               // Configuramos el pin en modo PWM
	LPC_PWM1->MR0 = 20000;                      // Periodo de la onda 
	LPC_PWM1->MR1 = 10000;                      // Periodo a nivel alto
	LPC_PWM1->LER |= ( 1 << 0 )|( 1 << 1 );     // Habilita la carga de un nuevo valor cuando se reinicia el MTC
	LPC_PWM1->TCR |= ( 1 << 0 )|( 1 << 3 ) ;    // Counter-PWM Enable

  
}

void pitido(void){
	
	LPC_PWM1->PCR |= ( 1 << 9 );					// Pin salida a 1
	osDelay (30);
	LPC_PWM1->PCR &= ~( 1 << 9 );					// Pin salida a 0
	
}

void pitidoTarjetaOut(void){
	
	LPC_PWM1->PCR |= ( 1 << 9 );					// Pin salida a 1
	osDelay (700);
	LPC_PWM1->PCR &= ~( 1 << 9 );					// Pin salida a 0
	
}


