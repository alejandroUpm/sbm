#include "LCD.h"
#include "I2C.h"
#include "cmsis_os.h" 
#include "constants.h"
#include "GPIO_LPC17xx.h"
#include "PIN_LPC17xx.h"
#include "LPC17xx.h"
#include "UART.h"
#include "ADC.h"
#include "ctype.h"
#include "stdio.h"
#include "string.h"
#include "PWM.h" 


extern osThreadId tid_estados;
extern osThreadId tid_JS;


void init_peripherals(void){
	//initialization for the LCD screen
	init_SPI();
	LCD_reset();
	Init_I2C();	
  Init_USART();
  init_ADC();
	init_PWM();
	
	// inicialization de los LED RGB
	GPIO_SetDir(PUERTO_RGB,RED,GPIO_DIR_OUTPUT);
	PIN_Configure(PUERTO_RGB,RED,PIN_FUNC_0,PIN_PINMODE_PULLDOWN,PIN_PINMODE_NORMAL);
	GPIO_SetDir(PUERTO_RGB,BLUE,GPIO_DIR_OUTPUT);
	PIN_Configure(PUERTO_RGB,BLUE,PIN_FUNC_0,PIN_PINMODE_PULLDOWN,PIN_PINMODE_NORMAL);
	GPIO_SetDir(PUERTO_RGB,GREEN,GPIO_DIR_OUTPUT);
	PIN_Configure(PUERTO_RGB,GREEN,PIN_FUNC_0,PIN_PINMODE_PULLDOWN,PIN_PINMODE_NORMAL);
  
  //inicialization of the JS
	PIN_Configure(PUERTO_JS,CENTER,PIN_FUNC_0,PIN_PINMODE_PULLDOWN,PIN_PINMODE_NORMAL);
	PIN_Configure(PUERTO_JS,DOWN,PIN_FUNC_0,PIN_PINMODE_PULLDOWN,PIN_PINMODE_NORMAL);
	PIN_Configure(PUERTO_JS,UP,PIN_FUNC_0,PIN_PINMODE_PULLDOWN,PIN_PINMODE_NORMAL);
	PIN_Configure(PUERTO_JS,LEFT,PIN_FUNC_0,PIN_PINMODE_PULLDOWN,PIN_PINMODE_NORMAL);
	PIN_Configure(PUERTO_JS,RIGHT,PIN_FUNC_0,PIN_PINMODE_PULLDOWN,PIN_PINMODE_NORMAL);
	
	//clear the flags on the first place (to make sure)
	LPC_GPIOINT->IO0IntClr |= 1 << LEFT;
	LPC_GPIOINT->IO0IntClr |= 1 << DOWN;
	LPC_GPIOINT->IO0IntClr |= 1 << UP;
	LPC_GPIOINT->IO0IntClr |= 1 << CENTER;
	LPC_GPIOINT->IO0IntClr |= 1 << RIGHT;
	
	// enables the interruption on the rise
	LPC_GPIOINT->IO0IntEnR |= 1 << CENTER;
  LPC_GPIOINT->IO0IntEnF |= 1 << CENTER; 	
	LPC_GPIOINT->IO0IntEnR |= 1 << DOWN; 
	LPC_GPIOINT->IO0IntEnR |= 1 << UP; 
	LPC_GPIOINT->IO0IntEnR |= 1 << LEFT;
	LPC_GPIOINT->IO0IntEnR |= 1 << RIGHT;		
	
	NVIC_EnableIRQ(EINT3_IRQn);
}

void EINT3_IRQHandler(void)
{
  if (LPC_GPIOINT->IO0IntStatR & (1 << LEFT)){
		osDelay(100);
    osSignalSet(tid_JS,PULSACION_LEFT);
    LPC_GPIOINT->IO0IntClr |= 1 << LEFT;
  }
    
  //gestionar el flanco 1 segundo en el rise de la pulsacion (final)
  if(LPC_GPIOINT->IO0IntStatR & (1 << CENTER)){
		osDelay(200);
		osSignalSet(tid_estados,PULSACION_CENTER);
    LPC_GPIOINT->IO0IntClr |= 1 << CENTER;    
  }
	
	if(LPC_GPIOINT->IO0IntStatF & (1 << CENTER)){
		osDelay(100);
		osSignalSet(tid_JS,PULSACION_CENTER_CORTA);
    LPC_GPIOINT->IO0IntClr |= 1 << CENTER;    
  }
  
  if(LPC_GPIOINT->IO0IntStatR & (1 << UP)){
		osDelay(100);
    osSignalSet(tid_JS,PULSACION_UP);
    LPC_GPIOINT->IO0IntClr |= 1 << UP;    
  }
  
  if(LPC_GPIOINT->IO0IntStatR & (1 << DOWN)){
		osDelay(100);
    osSignalSet(tid_JS,PULSACION_DOWN);
    LPC_GPIOINT->IO0IntClr |= 1 << DOWN;    
  } 
  
  if(LPC_GPIOINT->IO0IntStatR & (1 << RIGHT)){
		osDelay(100);
    osSignalSet(tid_JS,PULSACION_RIGHT);
    LPC_GPIOINT->IO0IntClr |= 1 << RIGHT;
  }
	
	if(LPC_GPIOINT->IO0IntStatF & (1 << RIGHT) || LPC_GPIOINT->IO0IntStatF & (1 << LEFT) ||
		 LPC_GPIOINT->IO0IntStatF & (1 << UP) || LPC_GPIOINT->IO0IntStatF & (1 << DOWN) ||
		 LPC_GPIOINT->IO0IntStatF & (1 << CENTER)){
			 osDelay(100);
  }	
}


void reset_RGB(void){
	GPIO_PinWrite(PUERTO_RGB,RED,1);
	GPIO_PinWrite(PUERTO_RGB,BLUE,1);
	GPIO_PinWrite(PUERTO_RGB,GREEN,1);
}

void convertToStringRX (char* input, char* output){
	
	int i,loop = 0;
	
	while(i < 30)
	{
			sprintf((char*)(output+i)," %02X", input[loop]);
			loop++;
			i+=3;
	}
}

void convertToStringTX (char* input, char* output){
	
	int i,loop = 0;
	
	while(i < 24)
	{
			sprintf((char*)(output+i)," %02X", input[loop]);
			loop++;
			i+=3;
	}
}




