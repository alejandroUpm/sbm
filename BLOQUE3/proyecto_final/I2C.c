
#include "Driver_I2C.h"
#include "constants.h"
#include "PIN_LPC17xx.h"
#include "GPIO_LPC17xx.h"

extern ARM_DRIVER_I2C							Driver_I2C2;
static ARM_DRIVER_I2C *I2Cdrv =  &Driver_I2C2;

uint8_t reg = 0x00;
uint8_t datos[DATOS_TEMPERATURA];
uint16_t temp_data;
float temp;

void Init_I2C(void){
	
	datos[0] = 0x00;
	
	I2Cdrv->Initialize(NULL);
	I2Cdrv->PowerControl(ARM_POWER_FULL);
	I2Cdrv->Control(ARM_I2C_BUS_SPEED,ARM_I2C_BUS_SPEED_FAST);
	I2Cdrv->Control(ARM_I2C_BUS_CLEAR,0);
	
}

float read_temp(void){
	
	I2Cdrv->MasterTransmit(LM75B_ADDR,&reg,1,true);
	while(I2Cdrv->GetStatus().busy);                //cambiar por  una se�al
	I2Cdrv->MasterReceive(LM75B_ADDR,datos,DATOS_TEMPERATURA,false);
	while(I2Cdrv->GetStatus().busy);
	
	temp_data = (datos[0] << 8 | datos[1]) >> 5;

	temp = temp_data * 0.125;

	return temp;
	
}










