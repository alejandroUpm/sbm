
// FUNCTIONS THAT INITIALISES ALL THE PERIPHERALS TO USE
void init_peripherals(void);

// FUNCTION WHICH MANAGES THE JS INTERRUTIONS 
void EINT3_IRQHandler(void);

//RESETS THE RGB LEDs  
void reset_RGB(void);

//CONVERTS A N BIT STRING INTO A PRINTABLE STRING
void convertToStringRX (char* input, char* output);

//CONVERTS A N BIT STRING INTO A PRINTABLE STRING
void convertToStringTX (char* input, char* output);
