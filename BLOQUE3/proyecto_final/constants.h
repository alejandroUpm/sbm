//ctes leds
#define PUERTO_LED    					1
#define LED_1         					18	
#define LED_2         					20
#define LED_3         					21
#define LED_4         					23

// CTE PARA EL PERIFERICO SPI A 100 MHz		
#define SSP1_CLOCK_100MHz				20
// TAMA�O MAX BUFFER LCD 
#define BUFFER_MAX            	512

// CTES SPI
#define PUERTO_SPI							0
#define	PIN_RESET								8
#define PIN_A0									6
#define PIN_CS									18

//ctes JS
#define PUERTO_JS  							0
#define UP     									23					
#define DOWN										17    	
#define CENTER     							16		
#define RIGHT     							24		
#define LEFT			    					15		

// ctes I2C 
#define LM75B_ADDR 							0x48
#define DATOS_TEMPERATURA				2


// SE�ALES DE HILOS
#define PULSACION_LARGA         0x000001
#define SEGUNDO                 0x000002
#define PULSACION_LEFT					0x000004		
#define PULSACION_RIGHT					0x000008
#define PULSACION_CENTER				0x000010
#define PULSACION_UP						0x000020
#define PULSACION_DOWN					0x000040
#define REPRODUCE_PAUSA					0X000080
#define REPRODUCIENDO     			0X000100
#define PROGRAMA_MINUTO					0x000400
#define PROGRAMA_HORA						0x000800
#define PROGRAMA_SEGUNDO        0x001000
#define PULSACION_CENTER_CORTA	0x002000
#define SIN_TARJETA 						0x004000
#define CON_TARJETA							0x008000


// ESTADOS
#define MODO_REPOSO 						1
#define MODO_REPRODUCCION 			2		
#define MODO_PROGRAMA_HORA 			3	

// RGB LED
#define PUERTO_RGB							2
#define RED											3
#define BLUE										1
#define GREEN										2

// USART
#define ENVIADO                 10
#define RECIBIDO								11
#define MAX_BYTES								50
#define RX_BYTES								10
 				

//CONSTANTES ADC
#define PUERTO_VOLUMEN      		1
#define PIN_VOLUMEN         		31

//COMANDOS MP3
#define PAUSE										0X0e
#define PLAY										0X0d
#define NEXT										0X01
#define NEXT_FOLDER							0X0F
#define PREVIOUS								0X02
#define TARJETA_OUT							0x3b
#define TARJETA_IN							0x3a
#define FIN_CANCION							0x3d
#define SLEEP										0X0a
#define WAKE_UP									0X0b
#define RESET										0x0c
#define STANDBY									0x09
#define INIT										0x00

#define MAX_CANCIONES						4
#define MIN_CANCIONES						1

