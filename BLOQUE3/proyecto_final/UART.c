//puerto uart0 para el puerto USB de tera term
//puerto uart3 para P0.0 y P0.1 para el chip 
//Tx del chip al pin 10 y Rx al pin 9 del micro
//alimentacion da igual, esta preparado para 3.2 - 5.6 V
//(recordar cruce de pines R1 -> T2 y R2 -> T1)
#include "Driver_USART.h"
#include "stdio.h"
#include "string.h"
#include "UART.h"
#include "GPIO_LPC17xx.h"
#include "PIN_LPC17xx.h"
#include "LPC17xx.h"

extern ARM_DRIVER_USART   Driver_USART3;
static ARM_DRIVER_USART  *USARTdrv3 = &Driver_USART3;
  
extern ARM_DRIVER_USART   Driver_USART0;
static ARM_DRIVER_USART  *USARTdrv0 = &Driver_USART0;

void USART0_cb(uint32_t event);
void USART3_cb(uint32_t event);

void Init_USART(void){
  USARTdrv0->Initialize(USART0_cb);
  USARTdrv0->PowerControl(ARM_POWER_FULL);
  USARTdrv0->Control(ARM_USART_MODE_ASYNCHRONOUS |
                     ARM_USART_DATA_BITS_8       |
                     ARM_USART_PARITY_NONE       |
                     ARM_USART_STOP_BITS_1       |
                     ARM_USART_FLOW_CONTROL_NONE,4800);
  
  USARTdrv0->Control(ARM_USART_CONTROL_TX,1);
  USARTdrv0->Control(ARM_USART_CONTROL_RX,1);
  
	
	// cable naranja a pin 9 ------ cable amarillo a pin 10
	// cable verde a V0------------ cable blanco a GND
  USARTdrv3->Initialize(USART3_cb);
  USARTdrv3->PowerControl(ARM_POWER_FULL);
  USARTdrv3->Control(ARM_USART_MODE_ASYNCHRONOUS |
                     ARM_USART_DATA_BITS_8       |
                     ARM_USART_PARITY_NONE       |
                     ARM_USART_STOP_BITS_1       |
                     ARM_USART_FLOW_CONTROL_NONE,9600);
  
  USARTdrv3->Control(ARM_USART_CONTROL_TX,1);
  USARTdrv3->Control(ARM_USART_CONTROL_RX,1);
	   
}

void sendTeraTerm(char cadena[],int cmd){
    USARTdrv0->Send(cadena,cmd);
}

void ReceiveTeraTerm(char *cmd, int numBytes){
  USARTdrv0->Receive(cmd,numBytes);
}

void sendMP3(char cadena[],char cmd){
    USARTdrv3->Send(cadena,cmd);
}

void ReceiveMP3(char *cmd,int numBytes){
  USARTdrv3->Receive(cmd,numBytes);
}

// ESTRUCTURA CANCIONES 
//	1� carpeta:	
//		001.ELO - Last train to london
//		002.Panic at tthe disco - high hopes
//		003.joe satriani - revelation
//		004.Wild Cherry - Play that funky music
//
//	2� carpeta:
//		001.Arctic Monkeys - brianstorm
//		002.Iron Maiden - run to the hills
//		003.warrant - cherry pie
//		004.judas priest - living after midnight
//
// 	3� carpeta:
//		001.stevie wonder - superstition
// 		002.the who - dirty jobs
//		003.The Rolling Stones - satisfation
//		004.joe satriani - mind storm
