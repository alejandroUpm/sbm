#include "ctype.h"
#include "stdio.h"
#include "cmsis_os.h" 
#include "I2C.h" 
#include "LCD.h"
#include "GPIO_LPC17xx.h"
#include "PIN_LPC17xx.h"
#include "LPC17xx.h"
#include "constants.h"
#include "utils.h"
#include "UART.h"
#include "Driver_USART.h"
#include "ADC.h"
#include "string.h"
#include "PWM.h" 


extern ARM_DRIVER_USART  *USARTdrv0;


//variable global de estados
int static estado = MODO_REPOSO;

int static programa_min_seg_hora = PROGRAMA_SEGUNDO;

static int seg = 0;
static int min = 0;
static int horas = 0;

static int seg_aux = 0;
static int min_aux = 0;
static int horas_aux = 0;

static char bufferRx[RX_BYTES];
static char bufferTx[8]={0x7e, 0xff, 0x06, 0x0f, 0x00, 0x01, 0x01, 0xef};
static char Vol[8]={0x7e, 0xff, 0x06, 0x06, 0x00, 0x00, 0x00, 0xef};

static char estado_reproduccion = STANDBY;

static int puls_larga = 0;

// DECLARACION DE MUTEX
osMutexDef(uart_mutex);
osMutexId(id_uart_mutex);

osMutexDef(RX_mutex);
osMutexId(id_RX_mutex);

osMutexDef(TX_mutex);
osMutexId(id_TX_mutex);

osMutexDef(LCD1_mutex);
osMutexId(id_LCD1_mutex);



// THREADS
void LCD1 (void const *argument);                            
osThreadId tid_LCD1;                                          
osThreadDef (LCD1, osPriorityNormal, 1, 0);

void LCD2 (void const *argument);                            
osThreadId tid_LCD2;                                          
osThreadDef (LCD2, osPriorityNormal, 1, 0);

void reloj (void const *argument);                            
osThreadId tid_reloj;                                          
osThreadDef (reloj, osPriorityNormal, 1, 0);

void estados (void const *argument);
osThreadId tid_estados;
osThreadDef (estados, osPriorityNormal, 1, 0);

void RGB(void const *argument);
osThreadId tid_RGB;
osThreadDef (RGB, osPriorityNormal, 1, 0);

void JS (void const *argument);
osThreadId tid_JS;
osThreadDef (JS, osPriorityNormal, 1, 0);

void uartRX(void const *argument);
osThreadId tid_RX;
osThreadDef(uartRX,osPriorityNormal, 1, 0);

void uartTX(void const *argument);
osThreadId tid_TX;
osThreadDef(uartTX,osPriorityNormal, 1, 0);



//TIMERS
void tid_timer_seg_cb(void const *argument);
osTimerId tid_timer_seg;
osTimerDef(timer_seg,tid_timer_seg_cb);

void timer_pulsacion_larga_cb(void const *argument);
osTimerId tid_pulsacion_larga;
osTimerDef(pulsacion_larga,timer_pulsacion_larga_cb);


int Init_Thread (void) {
  //creacion mutexs
  id_uart_mutex = osMutexCreate(osMutex(uart_mutex)); 
	id_RX_mutex = osMutexCreate(osMutex(RX_mutex));
	id_TX_mutex = osMutexCreate(osMutex(TX_mutex));
	id_LCD1_mutex = osMutexCreate(osMutex(LCD1_mutex));
	
	
	// creacion timers
	tid_timer_seg = osTimerCreate(osTimer(timer_seg),osTimerPeriodic,NULL);
	tid_pulsacion_larga = osTimerCreate(osTimer(pulsacion_larga),osTimerOnce,NULL);
	
	// creacion threads
  tid_LCD1 = osThreadCreate (osThread(LCD1), NULL);
	tid_LCD2 = osThreadCreate (osThread(LCD2), NULL);
	tid_reloj = osThreadCreate (osThread(reloj), NULL);
  tid_estados = osThreadCreate(osThread(estados),NULL);
	tid_RGB = osThreadCreate(osThread(RGB),NULL);
	tid_JS = osThreadCreate(osThread(JS),NULL);
	tid_RX = osThreadCreate(osThread(uartRX),NULL);
	tid_TX = osThreadCreate(osThread(uartTX),NULL);
  
	
  if (!tid_timer_seg | !tid_LCD1 | !tid_LCD2 | !tid_reloj | !tid_estados
			| !tid_pulsacion_larga | !tid_RGB | !tid_TX | !tid_RX 
			| !tid_JS) 
	return(-1);
  
  return(0);
}


void LCD1(void const *argument) {
	
	osEvent signals;
		
	while(1){

			if (estado == MODO_REPOSO){
				
				signals = osSignalWait(0x00,osWaitForever);
				
				if (signals.value.signals == SEGUNDO){
					reset_RGB();
					limpiaBuffer(0);
					imprimeLCD("MR  SBM T: ",0);
					escribirNumeroDecimal(read_temp(),0);
					imprimeLCD(" C",0);
					copy_to_lcd(0);	

					osSignalSet(tid_LCD2,SEGUNDO);
				
				}
			}	
				
		if (estado == MODO_REPRODUCCION){

      osDelay(200);
			osSignalSet(tid_RGB,REPRODUCIENDO);
			limpiaBuffer(0);
      osMutexWait(id_LCD1_mutex,osWaitForever);
			imprimeLCD("  F:",0);
			escribirNumeroEntero(bufferTx[5],0);
			imprimeLCD(" C:",0);
			escribirNumeroEntero(bufferTx[6],0);
			imprimeLCD("   Vol: ",0);
      escribirNumeroEntero(volumen(),0);
      Vol[6] = volumen();
      sendMP3(Vol,8);
      osMutexRelease(id_LCD1_mutex);
			copy_to_lcd(0);
		}
		
		if (estado == MODO_PROGRAMA_HORA){
			signals = osSignalWait(0x00,osWaitForever);
				
			if (signals.value.signals == SEGUNDO){

				limpiaBuffer(0);
				imprimeLCD("MPH SBM T: ",0);
				escribirNumeroDecimal(read_temp(),0);
				imprimeLCD(" C",0);
				copy_to_lcd(0);			  	
			}
		}
	}
}

void LCD2(void const *argument) {
	
	osEvent signals;
	static int segundos = 0;
	static int minutos = 0;
	
	while(1){
		
		if (estado == MODO_REPOSO){
		
			signals = osSignalWait(0x00,osWaitForever);
				
			if (signals.value.signals == SEGUNDO){

				osDelay(100);
				limpiaBuffer(1);
				imprimeLCD("    ",1);
				if(horas < 10) escribirNumeroEntero(0,1);
				escribirNumeroEntero(horas,1);
				imprimeLCD(":",1);
				if(min < 10) escribirNumeroEntero(0,1);
				escribirNumeroEntero(min,1);
				imprimeLCD(":",1);
				if(seg < 10) escribirNumeroEntero(0,1);
				escribirNumeroEntero(seg,1);
				copy_to_lcd(1);
				
			}
		}	
		
			
		if (estado == MODO_REPRODUCCION){
			
			signals = osSignalWait(0x00,osWaitForever);
						
			if(signals.value.signals == 0x001000){
				segundos = 0;
				minutos = 0;
			}
			if(signals.value.signals == SEGUNDO){
				
				osDelay(100);
				limpiaBuffer(1);
				imprimeLCD("      ",1);
				if(minutos<10) escribirNumeroEntero(0,1);
				escribirNumeroEntero(minutos,1);
				imprimeLCD(":",1);
				if(segundos<10) escribirNumeroEntero(0,1);
				escribirNumeroEntero(segundos,1);
				copy_to_lcd(1);	
				
				if(estado_reproduccion == PLAY){
					segundos++;
					if(segundos == 60){
						segundos = 0;
						minutos++;
					}
				}else if(estado_reproduccion == TARJETA_OUT){
					segundos = 0;
					minutos = 0;
				}
				
			}							
		}
		
		if (estado == MODO_PROGRAMA_HORA){
			
			osDelay(100);
			limpiaBuffer(1);
			imprimeLCD("    ",1);
			if(horas_aux < 10) escribirNumeroEntero(0,1);
			escribirNumeroEntero(horas_aux,1);
			imprimeLCD(":",1);
			if(min_aux < 10) escribirNumeroEntero(0,1);
			escribirNumeroEntero(min_aux,1);
			imprimeLCD(":",1);
			if(seg_aux < 10) escribirNumeroEntero(0,1);
			escribirNumeroEntero(seg_aux,1);
			copy_to_lcd(1);

		}
		
		osThreadYield ();
	}
}

void reloj(void const *argument) {
	
	osTimerStart(tid_timer_seg,1000);
	
	osEvent signals;
	
	while(1){		

		signals = osSignalWait(0x00,osWaitForever);
		if (estado != MODO_PROGRAMA_HORA){
			if (signals.value.signals == SEGUNDO){
				
				if(seg == 59 && min == 59 && horas == 23 ){
					seg = 0;
					min = 0;
					horas = 0;
				}else if(seg == 59 && min == 59){
					horas++;
					seg = 0;
					min = 0;
				}else if(seg == 59){
					min++;
					seg = 0;
				}else{
					seg++;
				}
			}
		}
	}
}

void JS (void const *argument) {
	
	osEvent signals;
	
	while(1){
		
		signals = osSignalWait(0x00,osWaitForever);
		
		if(signals.value.signals == PULSACION_CENTER_CORTA){
			pitido();
      if (puls_larga == 0){
        if (estado == MODO_REPRODUCCION && estado_reproduccion != TARJETA_OUT){
          osSignalSet(tid_TX,PULSACION_CENTER);	
        }else if (estado == MODO_PROGRAMA_HORA){				
            min = min_aux;
            horas = horas_aux;
            seg = seg_aux;
        }
      }
      puls_larga = 0;
    }
		
		if(signals.value.signals == PULSACION_UP){
      pitido();
			if (estado == MODO_REPRODUCCION && estado_reproduccion != TARJETA_OUT){
				osSignalSet(tid_TX,PULSACION_UP);
			}else if (estado == MODO_PROGRAMA_HORA){				
				if(programa_min_seg_hora == PROGRAMA_SEGUNDO){
          seg_aux = seg_aux + 5;
					if (seg_aux > 59) seg_aux = 0;
        }else if (programa_min_seg_hora == PROGRAMA_MINUTO){
					min_aux++;
					if (min_aux > 59) min_aux = 0;
				}else if (programa_min_seg_hora == PROGRAMA_HORA){
					horas_aux++;
					if(horas_aux > 23) horas_aux = 0;
				}
			}
		}
		
		if(signals.value.signals == PULSACION_DOWN){
      pitido();
			if (estado == MODO_REPRODUCCION && estado_reproduccion != TARJETA_OUT){
				osSignalSet(tid_TX,PULSACION_DOWN);
			}else if (estado == MODO_PROGRAMA_HORA){				
				if(programa_min_seg_hora == PROGRAMA_SEGUNDO){
          seg_aux = seg_aux - 5;
          if (seg_aux < 0) seg_aux = 59;
        }else if (programa_min_seg_hora == PROGRAMA_MINUTO){
					min_aux--;
					if (min_aux < 0) min_aux = 59;	
				}else if (programa_min_seg_hora == PROGRAMA_HORA){
					horas_aux--;
					if(horas_aux < 0) horas_aux = 23;
				}
			}
		}
		
		if(signals.value.signals == PULSACION_RIGHT){
      pitido();
			if (estado == MODO_REPRODUCCION && estado_reproduccion != TARJETA_OUT){
				osSignalSet(tid_TX,PULSACION_RIGHT);
			}else if (estado == MODO_PROGRAMA_HORA){				
        if(programa_min_seg_hora == PROGRAMA_SEGUNDO){
          programa_min_seg_hora = PROGRAMA_HORA;
        }else if (programa_min_seg_hora == PROGRAMA_MINUTO){
					programa_min_seg_hora = PROGRAMA_SEGUNDO;
				}else if (programa_min_seg_hora == PROGRAMA_HORA){
					programa_min_seg_hora = PROGRAMA_MINUTO;
				}
			}
		}
		
		if(signals.value.signals == PULSACION_LEFT){
      pitido();
			if (estado == MODO_REPRODUCCION && estado_reproduccion != TARJETA_OUT){
        osSignalSet(tid_TX,PULSACION_LEFT);
			}else if (estado == MODO_PROGRAMA_HORA){				
				if(programa_min_seg_hora == PROGRAMA_SEGUNDO){
          programa_min_seg_hora = PROGRAMA_MINUTO;
        }else if (programa_min_seg_hora == PROGRAMA_MINUTO){
					programa_min_seg_hora = PROGRAMA_HORA;
				}else if (programa_min_seg_hora == PROGRAMA_HORA){
					programa_min_seg_hora = PROGRAMA_SEGUNDO;
				}
			}
		}
		osThreadYield ();
	}
}


void estados (void const *argument) {
  
	osEvent signals;
		
  while(1){
		
		signals = osSignalWait(0x00,osWaitForever);
		
		if(signals.value.signals == PULSACION_CENTER){
      osTimerStart(tid_pulsacion_larga,1000);
    }
		
		if(signals.value.signals == PULSACION_LARGA){
			if (estado == MODO_REPOSO){
				estado = MODO_REPRODUCCION;
			}else if (estado == MODO_REPRODUCCION){
				estado = MODO_PROGRAMA_HORA;
			}else if (estado == MODO_PROGRAMA_HORA){				
				estado = MODO_REPOSO;
			}
		}		
											
    osThreadYield ();
  }
}


void RGB(void const *argument){
	
	static char OnOff = 0;
	
	while(1){
		if( estado_reproduccion == TARJETA_OUT){
				GPIO_PinWrite(PUERTO_RGB,GREEN,1);
				GPIO_PinWrite(PUERTO_RGB,RED,OnOff);
				GPIO_PinWrite(PUERTO_RGB,BLUE,1);
				OnOff ^= 0x01;
				osDelay(200);
		}else if(estado == MODO_REPRODUCCION && estado_reproduccion == PLAY){
				GPIO_PinWrite(PUERTO_RGB,GREEN,OnOff);
				GPIO_PinWrite(PUERTO_RGB,RED,1);
				GPIO_PinWrite(PUERTO_RGB,BLUE,1);
				OnOff ^= 0x01;
				osDelay(125);
		}else if (estado == MODO_REPRODUCCION && estado_reproduccion == PAUSE){
				GPIO_PinWrite(PUERTO_RGB,GREEN,1);
				GPIO_PinWrite(PUERTO_RGB,RED,1);
				GPIO_PinWrite(PUERTO_RGB,BLUE,OnOff);
				OnOff ^= 0x01;
				osDelay(1000);
		}else{
				reset_RGB();
		}
		osThreadYield ();
	}
}
  

void uartTX(void const *argument){
	
	osEvent signals;
	
	while(1){
    
		signals = osSignalWait(0X00,osWaitForever);
	
		if(signals.value.signals == PULSACION_CENTER){
      pitido();
			if (estado_reproduccion == PAUSE){
				bufferTx[3] = PLAY;
				osDelay(100);
				sendMP3(bufferTx,8);
				osDelay(100);
				estado_reproduccion = PLAY;
				osSignalSet(tid_LCD2,REPRODUCIENDO);
			}else if (estado_reproduccion == PLAY){
				bufferTx[3] = PAUSE;
				osDelay(100);
				sendMP3(bufferTx,8);
				osDelay(100);
				estado_reproduccion = PAUSE;
				osSignalSet(tid_LCD2,REPRODUCE_PAUSA);
			}else if(estado_reproduccion == STANDBY){
				bufferTx[3] = PLAY;
				osDelay(100);
				sendMP3(bufferTx,8);
				osDelay(150);
				estado_reproduccion = PLAY;				
			}
		}
		else if (signals.value.signals == PULSACION_LEFT){
			if(estado_reproduccion == PLAY || estado_reproduccion == PAUSE || estado_reproduccion == STANDBY){
				if (bufferTx[6] > MIN_CANCIONES) bufferTx[6] -= 0x01;
				bufferTx[3] = NEXT_FOLDER;
				osDelay(100);
				sendMP3(bufferTx,8);
				osDelay(100);
				estado_reproduccion = PLAY;
				osSignalSet(tid_LCD2,0x001000);
			}
    }
		else if (signals.value.signals == PULSACION_RIGHT){
			if(estado_reproduccion == PLAY || estado_reproduccion == PAUSE || estado_reproduccion == STANDBY){
				if (bufferTx[6] < MAX_CANCIONES) bufferTx[6] += 0x01;
				bufferTx[3] = NEXT_FOLDER;
				osDelay(100);
				sendMP3(bufferTx,8);
				osDelay(100);
				estado_reproduccion = PLAY;
				osSignalSet(tid_LCD2,0x001000);
			}
    }
		else if (signals.value.signals == PULSACION_UP){
			if(estado_reproduccion == PLAY || estado_reproduccion == PAUSE || estado_reproduccion == STANDBY){
				bufferTx[3] = NEXT_FOLDER;
				if (bufferTx[5] < 3) bufferTx[5] += 0x01;
				bufferTx[6] = 0x01;
				osDelay(100);
				sendMP3(bufferTx,8);
				osDelay(100);
				estado_reproduccion = PLAY;			
				osSignalSet(tid_LCD2,0x001000);
			}
    }
		else if (signals.value.signals == PULSACION_DOWN){
			if(estado_reproduccion == PLAY || estado_reproduccion == PAUSE || estado_reproduccion == STANDBY){
				bufferTx[3] = NEXT_FOLDER;
				if (bufferTx[5] > 1) bufferTx[5] -= 0x01;
				bufferTx[6] = 0x01;
				osDelay(100);
				sendMP3(bufferTx,8);
				osDelay(100);
				estado_reproduccion = PLAY;
				osSignalSet(tid_LCD2,0x001000);		
			}
    }	
		else if (signals.value.signals == SIN_TARJETA){
			estado_reproduccion = TARJETA_OUT;
      pitidoTarjetaOut();
			osMutexWait(id_TX_mutex,osWaitForever);
			bufferTx[3] = SLEEP;
			osDelay(100);
			sendMP3(bufferTx,8);
			osDelay(150);
		}
		else if(signals.value.signals == CON_TARJETA){
			bufferTx[3] = WAKE_UP;
      osMutexWait(id_TX_mutex,osWaitForever);
      osDelay(100);
      sendMP3(bufferTx,8);
      osDelay(150);
      osMutexRelease(id_TX_mutex);
			bufferTx[3] = STANDBY;    // seleccion de la  tarjeta  SD insertada
			osMutexWait(id_TX_mutex,osWaitForever);
      osDelay(150);
			sendMP3(bufferTx,8);
			osDelay(400);
			estado_reproduccion = STANDBY;
		}	
//		osMutexWait(id_TX_mutex,osWaitForever);
//		sprintf(buffer_aux1," TX %d:%d:%d --> ",horas,min,seg);
//		convertToStringTX(bufferTx,buffer_aux2);
//		strcat(buffer_aux1,buffer_aux2);
//		osDelay(150);
//		sendTeraTerm(buffer_aux1,47);
//		osDelay(300);
	}
}

void uartRX(void const *argument){
	
	char buffer_aux1[MAX_BYTES];
	char buffer_aux2[MAX_BYTES];
	
	while(1){
		osMutexWait(id_RX_mutex,osWaitForever);
		ReceiveMP3(bufferRx,10);
    osSignalWait(ENVIADO,osWaitForever);
		osMutexRelease(id_RX_mutex);
		if(bufferRx[3]==TARJETA_OUT){
			estado_reproduccion = TARJETA_OUT;
			estado = MODO_REPOSO;
			osSignalSet(tid_TX,SIN_TARJETA);
		}	
		if(bufferRx[3]==TARJETA_IN){
			estado = MODO_REPRODUCCION;
			osSignalSet(tid_TX,CON_TARJETA);
		}			
		if(bufferRx[3]==FIN_CANCION){
			estado_reproduccion = STANDBY ;
			osSignalSet(tid_LCD2,0x001000);
		}
		osMutexWait(id_RX_mutex,osWaitForever);
		sprintf(buffer_aux1," RX %d:%d:%d --> ",horas,min,seg);
		convertToStringRX(bufferRx,buffer_aux2);
		strcat(buffer_aux1,buffer_aux2);
		sendTeraTerm(buffer_aux1,47);
	}
}
	

// CALLBACKS DE LOS TIMERS
void tid_timer_seg_cb(void const *argument){
	osSignalSet(tid_reloj,SEGUNDO);
	osSignalSet(tid_LCD1,SEGUNDO);
	osSignalSet(tid_LCD2,SEGUNDO);
}


void timer_pulsacion_larga_cb(void const *argument){
  if(GPIO_PinRead(PUERTO_JS,CENTER)==1){
    osSignalSet(tid_estados,PULSACION_LARGA);
    puls_larga = 1;
  }
}

// CALLBACKS DE LOS USART
void USART0_cb(uint32_t event){
  
	if(event & ARM_USART_EVENT_SEND_COMPLETE){
		osMutexRelease(id_RX_mutex);
	}	
}


void USART3_cb(uint32_t event){
	
//	diferent flags
//	ARM_USART_EVENT_RECEIVE_COMPLETE		 	
//	ARM_USART_EVENT_TRANSFER_COMPLETE   	
//	ARM_USART_EVENT_SEND_COMPLETE			 		
//	ARM_USART_EVENT_TX_COMPLETE						
	if(event & ARM_USART_EVENT_RECEIVE_COMPLETE){
		osSignalSet(tid_RX,ENVIADO);
	}
	
	if(event & ARM_USART_EVENT_SEND_COMPLETE){
		osMutexRelease(id_TX_mutex);
	}
}


















// FUNCTIONS THAT INITIALISES ALL THE PERIPHERALS TO USE
void init_peripherals(void);

// FUNCTION WHICH MANAGES THE JS INTERRUTIONS 
void EINT3_IRQHandler(void);

//RESETS THE RGB LEDs  
void reset_RGB(void);

//CONVERTS A N BIT STRING INTO A PRINTABLE STRING
void convertToStringRX (char* input, char* output);

//CONVERTS A N BIT STRING INTO A PRINTABLE STRING
void convertToStringTX (char* input, char* output);
















#include "LCD.h"
#include "I2C.h"
#include "cmsis_os.h" 
#include "constants.h"
#include "GPIO_LPC17xx.h"
#include "PIN_LPC17xx.h"
#include "LPC17xx.h"
#include "UART.h"
#include "ADC.h"
#include "ctype.h"
#include "stdio.h"
#include "string.h"
#include "PWM.h" 


extern osThreadId tid_estados;
extern osThreadId tid_JS;


void init_peripherals(void){
	//initialization for the LCD screen
	init_SPI();
	LCD_reset();
	Init_I2C();	
  Init_USART();
  init_ADC();
	init_PWM();
	
	// inicialization de los LED RGB
	GPIO_SetDir(PUERTO_RGB,RED,GPIO_DIR_OUTPUT);
	PIN_Configure(PUERTO_RGB,RED,PIN_FUNC_0,PIN_PINMODE_PULLDOWN,PIN_PINMODE_NORMAL);
	GPIO_SetDir(PUERTO_RGB,BLUE,GPIO_DIR_OUTPUT);
	PIN_Configure(PUERTO_RGB,BLUE,PIN_FUNC_0,PIN_PINMODE_PULLDOWN,PIN_PINMODE_NORMAL);
	GPIO_SetDir(PUERTO_RGB,GREEN,GPIO_DIR_OUTPUT);
	PIN_Configure(PUERTO_RGB,GREEN,PIN_FUNC_0,PIN_PINMODE_PULLDOWN,PIN_PINMODE_NORMAL);
  
  //inicialization of the JS
	PIN_Configure(PUERTO_JS,CENTER,PIN_FUNC_0,PIN_PINMODE_PULLDOWN,PIN_PINMODE_NORMAL);
	PIN_Configure(PUERTO_JS,DOWN,PIN_FUNC_0,PIN_PINMODE_PULLDOWN,PIN_PINMODE_NORMAL);
	PIN_Configure(PUERTO_JS,UP,PIN_FUNC_0,PIN_PINMODE_PULLDOWN,PIN_PINMODE_NORMAL);
	PIN_Configure(PUERTO_JS,LEFT,PIN_FUNC_0,PIN_PINMODE_PULLDOWN,PIN_PINMODE_NORMAL);
	PIN_Configure(PUERTO_JS,RIGHT,PIN_FUNC_0,PIN_PINMODE_PULLDOWN,PIN_PINMODE_NORMAL);
	
	//clear the flags on the first place (to make sure)
	LPC_GPIOINT->IO0IntClr |= 1 << LEFT;
	LPC_GPIOINT->IO0IntClr |= 1 << DOWN;
	LPC_GPIOINT->IO0IntClr |= 1 << UP;
	LPC_GPIOINT->IO0IntClr |= 1 << CENTER;
	LPC_GPIOINT->IO0IntClr |= 1 << RIGHT;
	
	// enables the interruption on the rise
	LPC_GPIOINT->IO0IntEnR |= 1 << CENTER;
  LPC_GPIOINT->IO0IntEnF |= 1 << CENTER; 	
	LPC_GPIOINT->IO0IntEnR |= 1 << DOWN; 
	LPC_GPIOINT->IO0IntEnR |= 1 << UP; 
	LPC_GPIOINT->IO0IntEnR |= 1 << LEFT;
	LPC_GPIOINT->IO0IntEnR |= 1 << RIGHT;		
	
	NVIC_EnableIRQ(EINT3_IRQn);
}

void EINT3_IRQHandler(void)
{
  if (LPC_GPIOINT->IO0IntStatR & (1 << LEFT)){
		osDelay(100);
    osSignalSet(tid_JS,PULSACION_LEFT);
    LPC_GPIOINT->IO0IntClr |= 1 << LEFT;
  }
    
  //gestionar el flanco 1 segundo en el rise de la pulsacion (final)
  if(LPC_GPIOINT->IO0IntStatR & (1 << CENTER)){
		osDelay(150);
		osSignalSet(tid_estados,PULSACION_CENTER);
    LPC_GPIOINT->IO0IntClr |= 1 << CENTER;    
  }
	
	if(LPC_GPIOINT->IO0IntStatF & (1 << CENTER)){
		osDelay(100);
		osSignalSet(tid_JS,PULSACION_CENTER_CORTA);
    LPC_GPIOINT->IO0IntClr |= 1 << CENTER;    
  }
  
  if(LPC_GPIOINT->IO0IntStatR & (1 << UP)){
		osDelay(100);
    osSignalSet(tid_JS,PULSACION_UP);
    LPC_GPIOINT->IO0IntClr |= 1 << UP;    
  }
  
  if(LPC_GPIOINT->IO0IntStatR & (1 << DOWN)){
		osDelay(100);
    osSignalSet(tid_JS,PULSACION_DOWN);
    LPC_GPIOINT->IO0IntClr |= 1 << DOWN;    
  } 
  
  if(LPC_GPIOINT->IO0IntStatR & (1 << RIGHT)){
		osDelay(100);
    osSignalSet(tid_JS,PULSACION_RIGHT);
    LPC_GPIOINT->IO0IntClr |= 1 << RIGHT;
  }
	
	if(LPC_GPIOINT->IO0IntStatF & (1 << RIGHT) || LPC_GPIOINT->IO0IntStatF & (1 << LEFT) ||
		 LPC_GPIOINT->IO0IntStatF & (1 << UP) || LPC_GPIOINT->IO0IntStatF & (1 << DOWN) ||
		 LPC_GPIOINT->IO0IntStatF & (1 << CENTER)){
			 osDelay(100);
  }	
}


void reset_RGB(void){
	GPIO_PinWrite(PUERTO_RGB,RED,1);
	GPIO_PinWrite(PUERTO_RGB,BLUE,1);
	GPIO_PinWrite(PUERTO_RGB,GREEN,1);
}

void convertToStringRX (char* input, char* output){
	
	int i,loop = 0;
	
	while(i < 30)
	{
			sprintf((char*)(output+i)," %02X", input[loop]);
			loop++;
			i+=3;
	}
}

void convertToStringTX (char* input, char* output){
	
	int i,loop = 0;
	
	while(i < 24)
	{
			sprintf((char*)(output+i)," %02X", input[loop]);
			loop++;
			i+=3;
	}
}













#include "LPC17xx.h"
#include "PIN_LPC17xx.h"
#include "GPIO_LPC17xx.h"
#include "Driver_SPI.h"
#include "GPDMA_LPC17xx.h"


// RETRASA LOS MICROSEGUNDOS PASADOS POR PARAMETRO
// DE ENTRADA, CON CORRECCION DE RETRASO SOFTWARE/HARDWARE
extern void retraso (uint32_t microsegundos);

//CONFIGURA E INICIALIZA EL LCD
extern void init_SPI(void);

// ESCRITURA DE DATOS
extern void wr_data(unsigned char data);

// ESCRITURA DE COMANDO
extern void wr_cmd(unsigned char cmd);
	
// HACE RESET DEL LCD
extern void LCD_reset(void);

// COPIA EN EL LCD LO ALMACENADO EN EL BUFFER
extern void copy_to_lcd(int pagina);

// GENERA LA LETRA A
extern void letraA(unsigned char buf[]);

// ESCRIBE UN CARACTER EN LA LINEA 2 DEL LCD
extern int escribeLetra_L1(uint8_t letra);

// ESCRIBE UN CARACTER EN LA LINEA 2 DEL LCD
extern int escribeLetra_L2(uint8_t letra);

//EXCRIBE UN NUMERO ENTERO DE 32 BITS SIN SIGNO (numero) EN LA LINEA SELECCIONADA (linea)
extern void escribirNumeroEntero(uint32_t numero, int linea);

//EXCRIBE UN NUMERO DECIMAL(numero) EN LA LINEA SELECCIONADA (linea)
extern void escribirNumeroDecimal(float numero, int linea);

//IMPRIME POR PANTALLA DE LCD UN STRING COMPLETO (cadena) EN LA LINEA SELECCIONADA(linea)
void imprimeLCD(char cadena[], int linea);

//LIMPIA EL BUFFER 
void limpiaBuffer(int linea);
















#include "LPC17xx.h"
#include "PIN_LPC17xx.h"
#include "GPIO_LPC17xx.h"
#include "Driver_SPI.h"
#include "GPDMA_LPC17xx.h"
#include "LCD.h"
#include "Arial12x12.h"
#include "stdio.h"
#include "ctype.h"
#include "stdlib.h"
#include "string.h"
#include "constants.h"


// importacion del driver y asignacion a una variable 
extern ARM_DRIVER_SPI Driver_SPI1;
ARM_DRIVER_SPI* SPIdrv = &Driver_SPI1;
//factor por el que ha de multiplicarse el tiempo que queremos retrasar 
// pues el micro lleva por defecto un retraso hardware que hay que corregir
double variacion = 0.14;

// buffer donde se almacena la info a representar
char buffer[BUFFER_MAX]; 

// posiciones del buffer para iterarlo (solo dos debido a que las letra Arial van son 12x12)
// por lo tanto solo se pueden escribir dos lineas en total sin que se solapen porque la altura total es 32
int posicionL1 = 0;
int posicionL2 = 0;



void retraso (uint32_t microsegundos){

	for (int j = 0; j < variacion*microsegundos*1000 ; j++){}
}


void init_SPI(){
	
	
	//configuracion velocidad de periferico SPI
	LPC_SC->PCLKSEL0 |= 1 << SSP1_CLOCK_100MHz;
	// inicializa el driver SPI 
	SPIdrv->Initialize(NULL);
	// alimenta el periferico SPI 
	SPIdrv->PowerControl(ARM_POWER_FULL);
	// set up to master mode, Clock Polarity 1 y Clock Phase 1, Set the bit order from MSB to LSB,
	// Set the number of bits per SPI frame; range for n = 1..32 in our case  to 8
	SPIdrv->Control(ARM_SPI_MODE_MASTER |
									ARM_SPI_CPOL1_CPHA1	|
									ARM_SPI_MSB_LSB			|
									ARM_SPI_DATA_BITS(8),20000000);		// 20 MHz es la max frecuencia que puede soportar 
																										// el LCD
	
	// el master, osea el micro, es el que genera el cs de activacion del slave 
	GPIO_SetDir(PUERTO_SPI,PIN_CS,GPIO_DIR_OUTPUT);
	GPIO_PinWrite(PUERTO_SPI,PIN_CS,1);								
																										
	GPIO_SetDir(PUERTO_SPI,PIN_A0,GPIO_DIR_OUTPUT);
  GPIO_PinWrite(PUERTO_SPI,PIN_A0,1);
	
	GPIO_SetDir(PUERTO_SPI,PIN_RESET,GPIO_DIR_OUTPUT);
	GPIO_PinWrite(PUERTO_SPI,PIN_RESET,0);
	retraso(1);
	GPIO_PinWrite(PUERTO_SPI,PIN_RESET,1);
	retraso(1000);

}

// funcion de escritura de datos
void wr_data(unsigned char data){	
  GPIO_PinWrite(PUERTO_SPI,PIN_CS,0);
  GPIO_PinWrite(PUERTO_SPI,PIN_A0,1);
	SPIdrv->Send(&data,sizeof(data));
  GPIO_PinWrite(PUERTO_SPI,PIN_CS,1);
}

// funcion de escritura de comando
void wr_cmd(unsigned char cmd){
	GPIO_PinWrite(PUERTO_SPI,PIN_CS,0);
  GPIO_PinWrite(PUERTO_SPI,PIN_A0,0);
	SPIdrv->Send(&cmd,sizeof(cmd));
  GPIO_PinWrite(PUERTO_SPI,PIN_CS,1);
}
// reset de LCD
void LCD_reset(void)
{
    wr_cmd(0xAE); // Enciende o apaga el display --> cmd = 0xAE = 1010 111x -> si x = 0 led off; si x = 1 led on --> cmd = 0xAE = 1010 1110 --> display OFF
    wr_cmd(0xA2); // Estable la tension de polarización del LCD segun una relacion ed proporcionalidad -> cm= 0xA2 =  1010 001x --> si x = 0 relacion = 1/9; si x = 1 relacion = 1/7
    wr_cmd(0xA0); // Establece el direccionamiento de la RAM de datos del display -> cmd = 0xA0 = 1010 000x -> si x = 0 direccionamiento normal; si x = 1 direccionamiento inverso
    wr_cmd(0xC8); // Selecciona el modo de las salidas comunes COM --> cmd = 0xC8 = 1100 x--- -> si x = 0 modo normal de direccion; si x = 1 modo inverso de direccion
    wr_cmd(0x22); // Fija la relacion de resistencias internas --> cmd = 0x22 = 0010 0xxx -> xxx usados para selecionar la relacion de resistencias internas Rb/Ra --> en nuestro caso Rb/Ra = 2 --> cmd = 0x22 = 0010 0010
    wr_cmd(0x2F); // Seleciona el modo de operacion de la tension de entrada --> cmd = 0x2F = 0010 1xxx --> xxx seleccionan el modo de operacion --> cmd = 0x2F --> modo de tension de entrada normal; POWER ON
    wr_cmd(0x40); // Selecion de la direccion de la primera linea por la que empieza el display --> cmd = 0x40 = 01xx xxxx --> xx xxxx selecion de la direccion de la primera linea del display (de la 0 a la 32) --> cmd = 0x40 = 0100 0000 displat empieza por la linea 0
    wr_cmd(0xAF); // Display ON (mismo que primer comando)
    wr_cmd(0x81); // Fija el contraste
    wr_cmd(0x17);
		wr_cmd(0xA4); // Seleciona el modo en el que se muestrasn todos los puntos del display --> cmd = 0xA4 = 1010 010x --> si x = 0 modo normal; si x = 1 all points ON --> en nuestro caso seleccionamos el modo normal --> cmd = 0xA4 = 1010 0100
    wr_cmd(0xA6); // Seleciona el modo del display LCD --> cmd = 0xA6 = 1010 011x --> si x = 0 modo normal; si x = 1 modo inverso --> seleccionamos el modo normal --> cmd = 0xA6 = 1010 0110
}


// funcion que copia en el LCD lo almacenado en el buffer
void copy_to_lcd(int pagina){
	
    int i;
	
		if (pagina == 0){
			
				wr_cmd(0x00);      // 4 bits de la parte baja de la dirección a 0
				wr_cmd(0x10);      // 4 bits de la parte alta de la dirección a 0
				wr_cmd(0xB0);      // Página 0
				
				for(i=0;i<128;i++){
						wr_data(buffer[i]);
						}
	
				wr_cmd(0x00);      // 4 bits de la parte baja de la dirección a 0
				wr_cmd(0x10);      // 4 bits de la parte alta de la dirección a 0
				wr_cmd(0xB1);      // Página 1
				 
				for(i=128;i<256;i++){
						wr_data(buffer[i]);
						}
		
		}
		if (pagina == 1){	
			
				wr_cmd(0x00);       
				wr_cmd(0x10);      
				wr_cmd(0xB2);      //Página 2
			
				for(i=256;i<384;i++){
						wr_data(buffer[i]);
						}
			
			
				wr_cmd(0x00);       
				wr_cmd(0x10);       
				wr_cmd(0xB3);      // Pagina 3
				 				 
				for(i=384;i<512;i++){
						wr_data(buffer[i]);
						}
		}
}


void letraA(unsigned char buf[]){	
	
	
		unsigned char  aux[] = {0xC0,0x30,0x2C,0x23,0x23,0x2C,0x30,0xC0};
		for(int i = 0 ; i < sizeof(aux) ; i++){
			buf[i] = aux[i]; 			
		}
}


// funcion para escribir un caracter en la linea 2 del LCD 
int escribeLetra_L1(uint8_t letra){
	
	
	uint8_t 		i,valor1,valor2;
	uint16_t 		comienzo = 0;
	
  
  if (posicionL1 < 116){
    
    comienzo = 25 * (letra - ' ');
    
    for (i = 0 ; i < 12 ; i++){                     //limitar el numero de Bytes a escribir
      
      valor1 = Arial12x12[comienzo+i*2+1];
      valor2 = Arial12x12[comienzo+i*2+2];
      
      buffer[i+posicionL1] = valor1;
      buffer[i+128+posicionL1] = valor2;
    }
    posicionL1 = posicionL1 + Arial12x12[comienzo];
  }
	
	return 0;
}
// funcion para escribir un caracter en la linea 2 del LCD 
int escribeLetra_L2(uint8_t letra){
	uint8_t 		i,valor1,valor2;
	uint16_t 		comienzo = 0;
	
  //limitar el numero de Bytes a escribir
  if (posicionL2 < 116){
    
    comienzo = 25 * (letra - ' ');
    
    for (i = 0 ; i < 12 ; i++){
      
      valor1 = Arial12x12[comienzo+i*2+1];
      valor2 = Arial12x12[comienzo+i*2+2];
          
      buffer[i+256+posicionL2] = valor1;
      buffer[i+384+posicionL2] = valor2;
      
    }
    posicionL2 = posicionL2 + Arial12x12[comienzo];
  }
	
	
	return 0;
}
			

void escribirNumeroEntero(uint32_t numero,int linea){
	#define     BUFFERMAX 8
	
	char bufAux[BUFFERMAX];

  sprintf(bufAux,"%u",numero);	  
    	
  for(int i = 0 ; i < strlen(bufAux) ; i++){
    if (linea == 0){			
      escribeLetra_L1(bufAux[i]);
    }else if(linea == 1){
      escribeLetra_L2(bufAux[i]);
    }else{
			LCD_reset();
			imprimeLCD("ERROR",0);
    }
  }
}

void escribirNumeroDecimal(float numero, int linea){
  #define     BUFFERMAX 8
  
	char bufAux[BUFFERMAX];
  
  sprintf(bufAux,"%.1f",numero);	  
  
  for(int i = 0 ; i < strlen(bufAux) ; i++){
    if (linea == 0){
      escribeLetra_L1(bufAux[i]);
    }else if(linea == 1){
      escribeLetra_L2(bufAux[i]);
    }else{
			LCD_reset();
			imprimeLCD("ERROR",0);
    }
	}
}

//devuelve indice de byte
void imprimeLCD(char cadena[], int linea){
  
  for (int i = 0 ; i < strlen(cadena) ; i++){
    if (linea == 0){
      escribeLetra_L1(cadena[i]);
    }else if (linea == 1){
      escribeLetra_L2(cadena[i]);
    }
  }  
}

void limpiaBuffer(int linea){
	if (linea == 2){
		for (int i = 0 ; i<512 ; i++){
			buffer[i] = 0;
		}
		copy_to_lcd(0);
		copy_to_lcd(1);
		posicionL1 = 0;
		posicionL2 = 0;
	}
	if (linea == 0){
		for (int i = 0 ; i<255 ; i++){
			buffer[i] = 0;
		}
		copy_to_lcd(0);
		posicionL1 = 0;
	}
	if (linea == 1){
		for (int i = 256 ; i<512 ; i++){
			buffer[i] = 0;
		}
		copy_to_lcd(1);
		posicionL2 = 0;
	}
}

















				
const unsigned char Arial12x12[] = {

        0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char  
        0x02, 0x00, 0x00, 0x7F, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char !
        0x03, 0x07, 0x00, 0x00, 0x00, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char "
        0x07, 0x24, 0x00, 0xA4, 0x01, 0x7C, 0x00, 0xA7, 0x01, 0x7C, 0x00, 0x27, 0x00, 0x24, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char #
        0x06, 0x00, 0x00, 0xCE, 0x00, 0x11, 0x01, 0xFF, 0x03, 0x11, 0x01, 0xE2, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char $
        0x0A, 0x00, 0x00, 0x0E, 0x00, 0x11, 0x00, 0x11, 0x01, 0xCE, 0x00, 0x38, 0x00, 0xE6, 0x00, 0x11, 0x01, 0x10, 0x01, 0xE0, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char %
        0x08, 0x00, 0x00, 0xE0, 0x00, 0x1E, 0x01, 0x11, 0x01, 0x29, 0x01, 0xC6, 0x00, 0xA0, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char &
        0x02, 0x00, 0x00, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char '
        0x04, 0x00, 0x00, 0xF8, 0x00, 0x06, 0x03, 0x01, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char (
        0x03, 0x01, 0x04, 0x06, 0x03, 0xF8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char )
        0x05, 0x02, 0x00, 0x0A, 0x00, 0x07, 0x00, 0x0A, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char *
        0x06, 0x00, 0x00, 0x10, 0x00, 0x10, 0x00, 0x7C, 0x00, 0x10, 0x00, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char +
        0x02, 0x00, 0x00, 0x00, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char ,
        0x03, 0x20, 0x00, 0x20, 0x00, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char -
        0x02, 0x00, 0x00, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char .
        0x03, 0x80, 0x01, 0x7C, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char /
        0x06, 0x00, 0x00, 0xFE, 0x00, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0xFE, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char 0
        0x06, 0x00, 0x00, 0x04, 0x00, 0x02, 0x00, 0xFF, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char 1
        0x06, 0x00, 0x00, 0x02, 0x01, 0x81, 0x01, 0x41, 0x01, 0x31, 0x01, 0x0E, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char 2
        0x06, 0x00, 0x00, 0x82, 0x00, 0x01, 0x01, 0x11, 0x01, 0x11, 0x01, 0xEE, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char 3
        0x06, 0x00, 0x00, 0x60, 0x00, 0x58, 0x00, 0x46, 0x00, 0xFF, 0x01, 0x40, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char 4
        0x06, 0x00, 0x00, 0x9C, 0x00, 0x0B, 0x01, 0x09, 0x01, 0x09, 0x01, 0xF1, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char 5
        0x06, 0x00, 0x00, 0xFE, 0x00, 0x11, 0x01, 0x09, 0x01, 0x09, 0x01, 0xF2, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char 6
        0x06, 0x00, 0x00, 0x01, 0x00, 0xC1, 0x01, 0x39, 0x00, 0x07, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char 7
        0x06, 0x00, 0x00, 0xEE, 0x00, 0x11, 0x01, 0x11, 0x01, 0x11, 0x01, 0xEE, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char 8
        0x06, 0x00, 0x00, 0x9E, 0x00, 0x21, 0x01, 0x21, 0x01, 0x11, 0x01, 0xFE, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char 9
        0x02, 0x00, 0x00, 0x04, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char :
        0x02, 0x00, 0x00, 0x40, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char ;
        0x06, 0x00, 0x00, 0x10, 0x00, 0x28, 0x00, 0x28, 0x00, 0x44, 0x00, 0x44, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char <
        0x06, 0x48, 0x00, 0x48, 0x00, 0x48, 0x00, 0x48, 0x00, 0x48, 0x00, 0x48, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char =
        0x06, 0x00, 0x00, 0x44, 0x00, 0x44, 0x00, 0x28, 0x00, 0x28, 0x00, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char >
        0x06, 0x00, 0x00, 0x06, 0x00, 0x01, 0x00, 0x61, 0x01, 0x11, 0x00, 0x0E, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char ?
        0x0C, 0x00, 0x00, 0xF0, 0x01, 0x0C, 0x02, 0xE2, 0x04, 0x12, 0x09, 0x09, 0x09, 0x09, 0x09, 0xF1, 0x09, 0x19, 0x09, 0x02, 0x05, 0x86, 0x04, 0x78, 0x02,  // Code for char @
        0x07, 0x80, 0x01, 0x70, 0x00, 0x2E, 0x00, 0x21, 0x00, 0x2E, 0x00, 0x70, 0x00, 0x80, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char A
        0x07, 0x00, 0x00, 0xFF, 0x01, 0x11, 0x01, 0x11, 0x01, 0x11, 0x01, 0x11, 0x01, 0xFE, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char B
        0x08, 0x00, 0x00, 0x7C, 0x00, 0x82, 0x00, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x82, 0x00, 0x44, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char C
        0x08, 0x00, 0x00, 0xFF, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x82, 0x00, 0x7C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char D
        0x07, 0x00, 0x00, 0xFF, 0x01, 0x11, 0x01, 0x11, 0x01, 0x11, 0x01, 0x11, 0x01, 0x11, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char E
        0x06, 0x00, 0x00, 0xFF, 0x01, 0x11, 0x00, 0x11, 0x00, 0x11, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char F
        0x08, 0x00, 0x00, 0x7C, 0x00, 0x82, 0x00, 0x01, 0x01, 0x01, 0x01, 0x11, 0x01, 0x92, 0x00, 0x74, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char G
        0x08, 0x00, 0x00, 0xFF, 0x01, 0x10, 0x00, 0x10, 0x00, 0x10, 0x00, 0x10, 0x00, 0x10, 0x00, 0xFF, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char H
        0x02, 0x00, 0x00, 0xFF, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char I
        0x05, 0xC0, 0x00, 0x00, 0x01, 0x00, 0x01, 0x00, 0x01, 0xFF, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char J
        0x08, 0x00, 0x00, 0xFF, 0x01, 0x20, 0x00, 0x10, 0x00, 0x28, 0x00, 0x44, 0x00, 0x82, 0x00, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char K
        0x07, 0x00, 0x00, 0xFF, 0x01, 0x00, 0x01, 0x00, 0x01, 0x00, 0x01, 0x00, 0x01, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char L
        0x08, 0x00, 0x00, 0xFF, 0x01, 0x06, 0x00, 0x78, 0x00, 0x80, 0x01, 0x78, 0x00, 0x06, 0x00, 0xFF, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char M
        0x08, 0x00, 0x00, 0xFF, 0x01, 0x02, 0x00, 0x0C, 0x00, 0x10, 0x00, 0x60, 0x00, 0x80, 0x00, 0xFF, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char N
        0x08, 0x00, 0x00, 0x7C, 0x00, 0x82, 0x00, 0x01, 0x01, 0x01, 0x01, 0x01, 0x01, 0x82, 0x00, 0x7C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char O
        0x07, 0x00, 0x00, 0xFF, 0x01, 0x11, 0x00, 0x11, 0x00, 0x11, 0x00, 0x11, 0x00, 0x0E, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char P
        0x08, 0x00, 0x00, 0x7C, 0x00, 0x82, 0x00, 0x01, 0x01, 0x41, 0x01, 0x41, 0x01, 0x82, 0x00, 0x7C, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char Q
        0x08, 0x00, 0x00, 0xFF, 0x01, 0x11, 0x00, 0x11, 0x00, 0x11, 0x00, 0x31, 0x00, 0xD1, 0x00, 0x0E, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char R
        0x07, 0x00, 0x00, 0xCE, 0x00, 0x11, 0x01, 0x11, 0x01, 0x11, 0x01, 0x11, 0x01, 0xE6, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char S
        0x07, 0x01, 0x00, 0x01, 0x00, 0x01, 0x00, 0xFF, 0x01, 0x01, 0x00, 0x01, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char T
        0x08, 0x00, 0x00, 0x7F, 0x00, 0x80, 0x00, 0x00, 0x01, 0x00, 0x01, 0x00, 0x01, 0x80, 0x00, 0x7F, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char U
        0x07, 0x03, 0x00, 0x1C, 0x00, 0x60, 0x00, 0x80, 0x01, 0x60, 0x00, 0x1C, 0x00, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char V
        0x0B, 0x07, 0x00, 0x78, 0x00, 0x80, 0x01, 0x70, 0x00, 0x0E, 0x00, 0x01, 0x00, 0x0E, 0x00, 0x70, 0x00, 0x80, 0x01, 0x7C, 0x00, 0x03, 0x00, 0x00, 0x00,  // Code for char W
        0x07, 0x01, 0x01, 0xC6, 0x00, 0x28, 0x00, 0x10, 0x00, 0x28, 0x00, 0xC6, 0x00, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char X
        0x07, 0x01, 0x00, 0x06, 0x00, 0x08, 0x00, 0xF0, 0x01, 0x08, 0x00, 0x06, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char Y
        0x07, 0x00, 0x01, 0x81, 0x01, 0x61, 0x01, 0x11, 0x01, 0x0D, 0x01, 0x03, 0x01, 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char Z
        0x03, 0x00, 0x00, 0xFF, 0x07, 0x01, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char [
        0x03, 0x03, 0x00, 0x7C, 0x00, 0x80, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char BackSlash
        0x02, 0x01, 0x04, 0xFF, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char ]
        0x05, 0x18, 0x00, 0x06, 0x00, 0x01, 0x00, 0x06, 0x00, 0x18, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char ^
        0x07, 0x00, 0x04, 0x00, 0x04, 0x00, 0x04, 0x00, 0x04, 0x00, 0x04, 0x00, 0x04, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char _
        0x03, 0x00, 0x00, 0x01, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char `
        0x06, 0x00, 0x00, 0xC8, 0x00, 0x24, 0x01, 0x24, 0x01, 0xA4, 0x00, 0xF8, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char a
        0x06, 0x00, 0x00, 0xFF, 0x01, 0x88, 0x00, 0x04, 0x01, 0x04, 0x01, 0xF8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char b
        0x05, 0x00, 0x00, 0xF8, 0x00, 0x04, 0x01, 0x04, 0x01, 0x88, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char c
        0x06, 0x00, 0x00, 0xF8, 0x00, 0x04, 0x01, 0x04, 0x01, 0x08, 0x01, 0xFF, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char d
        0x06, 0x00, 0x00, 0xF8, 0x00, 0x24, 0x01, 0x24, 0x01, 0x24, 0x01, 0xB8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char e
        0x04, 0x04, 0x00, 0xFE, 0x01, 0x05, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char f
        0x06, 0x00, 0x00, 0xF8, 0x04, 0x04, 0x05, 0x04, 0x05, 0x88, 0x04, 0xFC, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char g
        0x06, 0x00, 0x00, 0xFF, 0x01, 0x08, 0x00, 0x04, 0x00, 0x04, 0x00, 0xF8, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char h
        0x02, 0x00, 0x00, 0xFD, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char i
        0x02, 0x00, 0x04, 0xFD, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char j
        0x06, 0x00, 0x00, 0xFF, 0x01, 0x20, 0x00, 0x30, 0x00, 0xC8, 0x00, 0x04, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char k
        0x02, 0x00, 0x00, 0xFF, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char l
        0x0A, 0x00, 0x00, 0xFC, 0x01, 0x08, 0x00, 0x04, 0x00, 0x04, 0x00, 0xF8, 0x01, 0x08, 0x00, 0x04, 0x00, 0x04, 0x00, 0xF8, 0x01, 0x00, 0x00, 0x00, 0x00,  // Code for char m
        0x06, 0x00, 0x00, 0xFC, 0x01, 0x08, 0x00, 0x04, 0x00, 0x04, 0x00, 0xF8, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char n
        0x06, 0x00, 0x00, 0xF8, 0x00, 0x04, 0x01, 0x04, 0x01, 0x04, 0x01, 0xF8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char o
        0x06, 0x00, 0x00, 0xFC, 0x07, 0x88, 0x00, 0x04, 0x01, 0x04, 0x01, 0xF8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char p
        0x06, 0x00, 0x00, 0xF8, 0x00, 0x04, 0x01, 0x04, 0x01, 0x88, 0x00, 0xFC, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char q
        0x04, 0x00, 0x00, 0xFC, 0x01, 0x08, 0x00, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char r
        0x06, 0x00, 0x00, 0x98, 0x00, 0x24, 0x01, 0x24, 0x01, 0x24, 0x01, 0xC8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char s
        0x03, 0x04, 0x00, 0xFF, 0x01, 0x04, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char t
        0x06, 0x00, 0x00, 0xFC, 0x00, 0x00, 0x01, 0x00, 0x01, 0x00, 0x01, 0xFC, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char u
        0x05, 0x0C, 0x00, 0x70, 0x00, 0x80, 0x01, 0x70, 0x00, 0x0C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char v
        0x09, 0x0C, 0x00, 0x70, 0x00, 0x80, 0x01, 0x70, 0x00, 0x0C, 0x00, 0x70, 0x00, 0x80, 0x01, 0x70, 0x00, 0x0C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char w
        0x05, 0x04, 0x01, 0xD8, 0x00, 0x20, 0x00, 0xD8, 0x00, 0x04, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char x
        0x05, 0x0C, 0x00, 0x70, 0x04, 0x80, 0x03, 0x70, 0x00, 0x0C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char y
        0x05, 0x04, 0x01, 0xC4, 0x01, 0x24, 0x01, 0x1C, 0x01, 0x04, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char z
        0x03, 0x20, 0x00, 0xDE, 0x03, 0x01, 0x04, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char {
        0x02, 0x00, 0x00, 0xFF, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char |
        0x04, 0x00, 0x00, 0x01, 0x04, 0xDE, 0x03, 0x20, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char }
        0x07, 0x00, 0x00, 0x20, 0x00, 0x10, 0x00, 0x10, 0x00, 0x20, 0x00, 0x20, 0x00, 0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,  // Code for char ~
        0x08, 0x00, 0x00, 0xFE, 0x01, 0x02, 0x01, 0x02, 0x01, 0x02, 0x01, 0x02, 0x01, 0x02, 0x01, 0xFE, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00   // Code for char 
        };


		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
// INITIALIZATION OF THE I2C MASTER/TEMP_SENSOR
void Init_I2C(void);

// READS THE DATA FROM THE TEMPERATURE SENSOR AND 
// CONVERT THE DATA TO NUMBERS
float read_temp(void);
















#include "Driver_I2C.h"
#include "constants.h"
#include "PIN_LPC17xx.h"
#include "GPIO_LPC17xx.h"

extern ARM_DRIVER_I2C							Driver_I2C2;
static ARM_DRIVER_I2C *I2Cdrv =  &Driver_I2C2;

uint8_t reg = 0x00;
uint8_t datos[DATOS_TEMPERATURA];
uint16_t temp_data;
float temp;

void Init_I2C(void){
	
	datos[0] = 0x00;
	
	I2Cdrv->Initialize(NULL);
	I2Cdrv->PowerControl(ARM_POWER_FULL);
	I2Cdrv->Control(ARM_I2C_BUS_SPEED,ARM_I2C_BUS_SPEED_FAST);
	I2Cdrv->Control(ARM_I2C_BUS_CLEAR,0);
	
}

float read_temp(void){
	
	I2Cdrv->MasterTransmit(LM75B_ADDR,&reg,1,true);
	while(I2Cdrv->GetStatus().busy);                //cambiar por  una señal
	I2Cdrv->MasterReceive(LM75B_ADDR,datos,DATOS_TEMPERATURA,false);
	while(I2Cdrv->GetStatus().busy);
	
	temp_data = (datos[0] << 8 | datos[1]) >> 5;

	temp = temp_data * 0.125;

	return temp;
	
}
















#include "stdint.h"
#include "utils.h"
#include "Driver_USART.h"

void USART0_cb(uint32_t event);
void USART3_cb(uint32_t event);

void Init_USART(void);

void sendTeraTerm(char cadena[],int cmd);

void ReceiveTeraTerm(char *cmd, int numBytes);

void sendMP3(char cadena[],char cmd);

void ReceiveMP3(char cmd[],int numBytes);














//puerto uart0 para el puerto USB de tera term
//puerto uart3 para P0.0 y P0.1 para el chip 
//Tx del chip al pin 10 y Rx al pin 9 del micro
//alimentacion da igual, esta preparado para 3.2 - 5.6 V
//(recordar cruce de pines R1 -> T2 y R2 -> T1)
#include "Driver_USART.h"
#include "stdio.h"
#include "string.h"
#include "UART.h"
#include "GPIO_LPC17xx.h"
#include "PIN_LPC17xx.h"
#include "LPC17xx.h"

extern ARM_DRIVER_USART   Driver_USART3;
static ARM_DRIVER_USART  *USARTdrv3 = &Driver_USART3;
  
extern ARM_DRIVER_USART   Driver_USART0;
static ARM_DRIVER_USART  *USARTdrv0 = &Driver_USART0;

void USART0_cb(uint32_t event);
void USART3_cb(uint32_t event);

void Init_USART(void){
  USARTdrv0->Initialize(USART0_cb);
  USARTdrv0->PowerControl(ARM_POWER_FULL);
  USARTdrv0->Control(ARM_USART_MODE_ASYNCHRONOUS |
                     ARM_USART_DATA_BITS_8       |
                     ARM_USART_PARITY_NONE       |
                     ARM_USART_STOP_BITS_1       |
                     ARM_USART_FLOW_CONTROL_NONE,4800);
  
  USARTdrv0->Control(ARM_USART_CONTROL_TX,1);
  USARTdrv0->Control(ARM_USART_CONTROL_RX,1);
  
	
	// cable naranja a pin 9 ------ cable amarillo a pin 10
	// cable verde a V0------------ cable blanco a GND
  USARTdrv3->Initialize(USART3_cb);
  USARTdrv3->PowerControl(ARM_POWER_FULL);
  USARTdrv3->Control(ARM_USART_MODE_ASYNCHRONOUS |
                     ARM_USART_DATA_BITS_8       |
                     ARM_USART_PARITY_NONE       |
                     ARM_USART_STOP_BITS_1       |
                     ARM_USART_FLOW_CONTROL_NONE,9600);
  
  USARTdrv3->Control(ARM_USART_CONTROL_TX,1);
  USARTdrv3->Control(ARM_USART_CONTROL_RX,1);
	   
}

void sendTeraTerm(char cadena[],int cmd){
    USARTdrv0->Send(cadena,cmd);
}

void ReceiveTeraTerm(char *cmd, int numBytes){
  USARTdrv0->Receive(cmd,numBytes);
}

void sendMP3(char cadena[],char cmd){
    USARTdrv3->Send(cadena,cmd);
}

void ReceiveMP3(char *cmd,int numBytes){
  USARTdrv3->Receive(cmd,numBytes);
}

// ESTRUCTURA CANCIONES 
//	1º carpeta:	
//		001.ELO - Last train to london
//		002.Panic at tthe disco - high hopes
//		003.joe satriani - revelation
//		004.Wild Cherry - Play that funky music
//
//	2º carpeta:
//		001.Arctic Monkeys - brianstorm
//		002.Iron Maiden - run to the hills
//		003.warrant - cherry pie
//		004.judas priest - living after midnight
//
// 	3º carpeta:
//		001.stevie wonder - superstition
// 		002.the who - dirty jobs
//		003.The Rolling Stones - satisfation
//		004.joe satriani - mind storm
















void init_ADC(void);

char volumen(void);
















#include "LCD.h"
#include "LPC17xx.h"


void init_ADC(void){
	
	// Inicializacion LCD
	init_SPI();
	LCD_reset();
	
	// Inicializacion ADC
	LPC_SC->PCONP |=  (1 << 12);								// Alimentamos el periferico ( tras el reset esta a 0 )
	LPC_ADC->ADCR |= (1 << 21);									// Habilita el ADC ( para apagarlo, primero deshabilitarlo )
	
	//PCLKSEL0 &= ~(11 << 24);									// Suministramos al periferico la señal del reloj del sistema / 4 (25MHz),
																							// tras el reset, estos bits estan a 00, por lo que no hay que modificar el valor del registro
	LPC_ADC->ADCR |= (1 << 8);									// Escalamos el reloj del ADC, diviendolo entre este valor(+1). Debe ser menor de 13MHz
																							// pero interesa que sea proximo : 25/(1+1) = 12.5MHz
	
	LPC_PINCON->PINSEL3 |= (0x3 << 30);					// Configuramos el pin 1.31 para la funcion AD0.5 
	LPC_ADC->ADCR |= (1 << 5);									// Habilita el pin 5 del ADC para su conversion
//	LPC_ADC->ADCR |= (1 << 16);
}

int volumen(void){
	
	uint32_t registro;
	int valor;
	
	LPC_ADC->ADCR |= (1 << 24);									// Iniciamos la conversion
	while((LPC_ADC->ADGDR & 1<<31)==0);					// Bit "DONE" del registro del canal se pone a 1 cuando se ha terminado la conversion, tras la lectura del dato el bit se pone a 0
	
	registro = (LPC_ADC ->ADGDR >> 4) & 0xFFF;
	valor = registro / (4096/30);
	
	
	return valor;
}


















//INITIALIZATION OF THE PWM
void init_PWM(void);

//FUNCTION WHICH  MAKES A SOUND
void pitido(void);

//FUNCTION WHICH  MAKES A SOUND
void pitidoTarjetaOut(void);















#include "LPC17xx.h"
#include "GPIO_LPC17xx.H"
#include "PIN_LPC17xx.h"
#include "cmsis_os.h" 
#include "PWM.h"

#define PUERTO_ALTAVOZ				2
#define PIN_ALTAVOZ						0

void init_PWM(void){

	//LPC_SC->PCONP |= ( 1 << 6 );              // Inicializamos el periferico. Tras el reset esta habilitado.
	LPC_SC->PCLKSEL0 |= ( 1 << 12 );            // Señal de reloj de sistema ( 100MHz )
	LPC_PINCON->PINSEL4 |= ( 1 );               // Configuramos el pin en modo PWM
	LPC_PWM1->MR0 = 20000;                      // Periodo de la onda 
	LPC_PWM1->MR1 = 10000;                      // Periodo a nivel alto
	LPC_PWM1->LER |= ( 1 << 0 )|( 1 << 1 );     // Habilita la carga de un nuevo valor cuando se reinicia el MTC
	LPC_PWM1->TCR |= ( 1 << 0 )|( 1 << 3 ) ;    // Counter-PWM Enable

  
}

void pitido(void){
	
	LPC_PWM1->PCR |= ( 1 << 9 );					// Pin salida a 1
	osDelay (50);
	LPC_PWM1->PCR &= ~( 1 << 9 );					// Pin salida a 0
	
}

void pitidoTarjetaOut(void){
	
	LPC_PWM1->PCR |= ( 1 << 9 );					// Pin salida a 1
	osDelay (700);
	LPC_PWM1->PCR &= ~( 1 << 9 );					// Pin salida a 0
	
}












/*----------------------------------------------------------------------------
 * CMSIS-RTOS 'main' function template
 *---------------------------------------------------------------------------*/

#define osObjectsPublic                     // define objects in main module
#include "osObjects.h"                      // RTOS object definitions
#include "utils.h"


extern int Init_Thread(void);
/*
 * main: initialize and start the system
 */
int main (void) {
  osKernelInitialize ();                    // initialize CMSIS-RTOS

  // initialize peripherals here
	Init_Thread();
	
	init_peripherals();
  // create 'thread' functions that start executing,
  // example: tid_name = osThreadCreate (osThread(name), NULL);

  osKernelStart ();                         // start thread execution 
}






