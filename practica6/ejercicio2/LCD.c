#include "LPC17xx.h"
#include "PIN_LPC17xx.h"
#include "GPIO_LPC17xx.h"
#include "Driver_SPI.h"
#include "GPDMA_LPC17xx.h"
#include "LCD.h"
#include "Arial12x12.h"
#include "stdio.h"
#include "ctype.h"
#include "stdlib.h"
#include "string.h"
#include "constants.h"


// importacion del driver y asignacion a una variable 
extern ARM_DRIVER_SPI Driver_SPI1;
ARM_DRIVER_SPI* SPIdrv = &Driver_SPI1;
//factor por el que ha de multiplicarse el tiempo que queremos retrasar 
// pues el micro lleva por defecto un retraso hardware que hay que corregir
double variacion = 0.14;

// buffer donde se almacena la info a representar
char buffer[BUFFER_MAX]; 

// posiciones del buffer para iterarlo (solo dos debido a que las letra Arial van son 12x12)
// por lo tanto solo se pueden escribir dos lineas en total sin que se solapen porque la altura total es 32
int posicionL1 = 0;
int posicionL2 = 0;



void retraso (uint32_t microsegundos){

	for (int j = 0; j < variacion*microsegundos*1000 ; j++){}
}


void init_SPI(){
	
	
	//configuracion velocidad de periferico SPI
	LPC_SC->PCLKSEL0 |= 1 << SSP1_CLOCK_100MHz;
	// inicializa el driver SPI 
	SPIdrv->Initialize(NULL);
	// alimenta el periferico SPI 
	SPIdrv->PowerControl(ARM_POWER_FULL);
	// set up to master mode, Clock Polarity 1 y Clock Phase 1, Set the bit order from MSB to LSB,
	// Set the number of bits per SPI frame; range for n = 1..32 in our case  to 8
	SPIdrv->Control(ARM_SPI_MODE_MASTER |
									ARM_SPI_CPOL1_CPHA1	|
									ARM_SPI_MSB_LSB			|
									ARM_SPI_DATA_BITS(8),20000000);		// 20 MHz es la max frecuencia que puede soportar 
																										// el LCD
	
	// el master, osea el micro, es el que genera el cs de activacion del slave 
	GPIO_SetDir(PUERTO_SPI,PIN_CS,GPIO_DIR_OUTPUT);
	GPIO_PinWrite(PUERTO_SPI,PIN_CS,1);								
																										
	GPIO_SetDir(PUERTO_SPI,PIN_A0,GPIO_DIR_OUTPUT);
  GPIO_PinWrite(PUERTO_SPI,PIN_A0,1);
	
	GPIO_SetDir(PUERTO_SPI,PIN_RESET,GPIO_DIR_OUTPUT);
	GPIO_PinWrite(PUERTO_SPI,PIN_RESET,0);
	retraso(1);
	GPIO_PinWrite(PUERTO_SPI,PIN_RESET,1);
	retraso(1000);

}

// funcion de escritura de datos
void wr_data(unsigned char data){	
  GPIO_PinWrite(PUERTO_SPI,PIN_CS,0);
  GPIO_PinWrite(PUERTO_SPI,PIN_A0,1);
	SPIdrv->Send(&data,sizeof(data));
  GPIO_PinWrite(PUERTO_SPI,PIN_CS,1);
}

// funcion de escritura de comando
void wr_cmd(unsigned char cmd){
	GPIO_PinWrite(PUERTO_SPI,PIN_CS,0);
  GPIO_PinWrite(PUERTO_SPI,PIN_A0,0);
	SPIdrv->Send(&cmd,sizeof(cmd));
  GPIO_PinWrite(PUERTO_SPI,PIN_CS,1);
}
// reset de LCD
void LCD_reset(void)
{
    wr_cmd(0xAE); // Enciende o apaga el display --> cmd = 0xAE = 1010 111x -> si x = 0 led off; si x = 1 led on --> cmd = 0xAE = 1010 1110 --> display OFF
    wr_cmd(0xA2); // Estable la tension de polarizaci�n del LCD segun una relacion ed proporcionalidad -> cm= 0xA2 =  1010 001x --> si x = 0 relacion = 1/9; si x = 1 relacion = 1/7
    wr_cmd(0xA0); // Establece el direccionamiento de la RAM de datos del display -> cmd = 0xA0 = 1010 000x -> si x = 0 direccionamiento normal; si x = 1 direccionamiento inverso
    wr_cmd(0xC8); // Selecciona el modo de las salidas comunes COM --> cmd = 0xC8 = 1100 x--- -> si x = 0 modo normal de direccion; si x = 1 modo inverso de direccion
    wr_cmd(0x22); // Fija la relacion de resistencias internas --> cmd = 0x22 = 0010 0xxx -> xxx usados para selecionar la relacion de resistencias internas Rb/Ra --> en nuestro caso Rb/Ra = 2 --> cmd = 0x22 = 0010 0010
    wr_cmd(0x2F); // Seleciona el modo de operacion de la tension de entrada --> cmd = 0x2F = 0010 1xxx --> xxx seleccionan el modo de operacion --> cmd = 0x2F --> modo de tension de entrada normal; POWER ON
    wr_cmd(0x40); // Selecion de la direccion de la primera linea por la que empieza el display --> cmd = 0x40 = 01xx xxxx --> xx xxxx selecion de la direccion de la primera linea del display (de la 0 a la 32) --> cmd = 0x40 = 0100 0000 displat empieza por la linea 0
    wr_cmd(0xAF); // Display ON (mismo que primer comando)
    wr_cmd(0x81); // Fija el contraste
    wr_cmd(0x17);
		wr_cmd(0xA4); // Seleciona el modo en el que se muestrasn todos los puntos del display --> cmd = 0xA4 = 1010 010x --> si x = 0 modo normal; si x = 1 all points ON --> en nuestro caso seleccionamos el modo normal --> cmd = 0xA4 = 1010 0100
    wr_cmd(0xA6); // Seleciona el modo del display LCD --> cmd = 0xA6 = 1010 011x --> si x = 0 modo normal; si x = 1 modo inverso --> seleccionamos el modo normal --> cmd = 0xA6 = 1010 0110
}


// funcion que copia en el LCD lo almacenado en el buffer
void copy_to_lcd(int pagina){
	
    int i;
	
		if (pagina == 0){
			
				wr_cmd(0x00);      // 4 bits de la parte baja de la direcci�n a 0
				wr_cmd(0x10);      // 4 bits de la parte alta de la direcci�n a 0
				wr_cmd(0xB0);      // P�gina 0
				
				for(i=0;i<128;i++){
						wr_data(buffer[i]);
						}
	
				wr_cmd(0x00);      // 4 bits de la parte baja de la direcci�n a 0
				wr_cmd(0x10);      // 4 bits de la parte alta de la direcci�n a 0
				wr_cmd(0xB1);      // P�gina 1
				 
				for(i=128;i<256;i++){
						wr_data(buffer[i]);
						}
		
		}
		if (pagina == 1){	
			
				wr_cmd(0x00);       
				wr_cmd(0x10);      
				wr_cmd(0xB2);      //P�gina 2
			
				for(i=256;i<384;i++){
						wr_data(buffer[i]);
						}
			
			
				wr_cmd(0x00);       
				wr_cmd(0x10);       
				wr_cmd(0xB3);      // Pagina 3
				 				 
				for(i=384;i<512;i++){
						wr_data(buffer[i]);
						}
		}
}


void letraA(unsigned char buf[]){	
	
	
		unsigned char  aux[] = {0xC0,0x30,0x2C,0x23,0x23,0x2C,0x30,0xC0};
		for(int i = 0 ; i < sizeof(aux) ; i++){
			buf[i] = aux[i]; 			
		}
}


// funcion para escribir un caracter en la linea 2 del LCD 
int escribeLetra_L1(uint8_t letra){
	
	
	uint8_t 		i,valor1,valor2;
	uint16_t 		comienzo = 0;
	
  
  if (posicionL1 < 116){
    
    comienzo = 25 * (letra - ' ');
    
    for (i = 0 ; i < 12 ; i++){                     //limitar el numero de Bytes a escribir
      
      valor1 = Arial12x12[comienzo+i*2+1];
      valor2 = Arial12x12[comienzo+i*2+2];
      
      buffer[i+posicionL1] = valor1;
      buffer[i+128+posicionL1] = valor2;
    }
    posicionL1 = posicionL1 + Arial12x12[comienzo];
  }
	
	return 0;
}
// funcion para escribir un caracter en la linea 2 del LCD 
int escribeLetra_L2(uint8_t letra){
	uint8_t 		i,valor1,valor2;
	uint16_t 		comienzo = 0;
	
  //limitar el numero de Bytes a escribir
  if (posicionL2 < 116){
    
    comienzo = 25 * (letra - ' ');
    
    for (i = 0 ; i < 12 ; i++){
      
      valor1 = Arial12x12[comienzo+i*2+1];
      valor2 = Arial12x12[comienzo+i*2+2];
          
      buffer[i+256+posicionL2] = valor1;
      buffer[i+384+posicionL2] = valor2;
      
    }
    posicionL2 = posicionL2 + Arial12x12[comienzo];
  }
	
	
	return 0;
}
			

void escribirNumeroEntero(uint32_t numero,int linea){
	#define     BUFFERMAX 8
	
	char bufAux[BUFFERMAX];

  sprintf(bufAux,"%u",numero);	  
    	
  for(int i = 0 ; i < strlen(bufAux) ; i++){
    if (linea == 0){			
      escribeLetra_L1(bufAux[i]);
    }else if(linea == 1){
      escribeLetra_L2(bufAux[i]);
    }else{
			LCD_reset();
			imprimeLCD("ERROR",0);
    }
  }
}

void escribirNumeroDecimal(int numero, int linea){
  #define     BUFFERMAX 8
  
	char bufAux[BUFFERMAX];
  
  sprintf(bufAux,"%d",numero);	  
  
  for(int i = 0 ; i < strlen(bufAux) ; i++){
    if (linea == 0){
      escribeLetra_L1(bufAux[i]);
    }else if(linea == 1){
      escribeLetra_L2(bufAux[i]);
    }else{
			LCD_reset();
			imprimeLCD("ERROR",0);
    }
	}
}

//devuelve indice de byte
void imprimeLCD(char cadena[], int linea){
  
  for (int i = 0 ; i < strlen(cadena) ; i++){
    if (linea == 0){
      escribeLetra_L1(cadena[i]);
    }else if (linea == 1){
      escribeLetra_L2(cadena[i]);
    }
  }  
}

void limpiaBuffer(void){
//	for (int i = 0 ; i<sizeof(buffer) ; i++){
//		buffer[i] = NULL;
//	}
//	imprimeLCD("                    ",0);
//	copy_to_lcd(0);
//	imprimeLCD("                    ",1);
//	copy_to_lcd(1);
//	posicionL1 = 0;
//  posicionL2 = 0;
		
	for (int i = 0 ; i<512 ; i++){
		buffer[i] = 0;
	}
	copy_to_lcd(0);
	copy_to_lcd(1);
	posicionL1 = 0;
  posicionL2 = 0;
}
