
#include "cmsis_os.h"                                           // CMSIS RTOS header file
#include "LCD.h"
#include "utils.h"
#include "stdio.h"
#include "constants.h"



/*----------------------------------------------------------------------------
 *      Thread 1 'Thread_Name': Sample thread
 *---------------------------------------------------------------------------*/
 
void cronografo (void const *argument);                             // thread function
osThreadId tid_cronografo;                                          // thread id
osThreadDef (cronografo, osPriorityNormal, 1, 0);                   // thread object

void tiempo_LCD (void const *argument);
osThreadId tid_tiempo_LCD;
osThreadDef (tiempo_LCD, osPriorityNormal, 1, 0);

void thread_papadeo(void const *argument);
osThreadId tid_thread_papadeo;
osThreadDef (thread_papadeo, osPriorityNormal, 1, 0);

void  timer1_seg_Callback(void const *argument);
osTimerDef(timer1_seg,timer1_seg_Callback);
osTimerId timer_1seg_id;

void  parpadeo_Callback(void const *argument);
osTimerDef(parpadeo,parpadeo_Callback);
osTimerId parpadeo_id;

int Init_Thread (void) {
	timer_1seg_id = osTimerCreate(osTimer(timer1_seg),osTimerPeriodic ,NULL);
	parpadeo_id = osTimerCreate(osTimer(parpadeo),osTimerOnce ,NULL);
	
  tid_cronografo = osThreadCreate (osThread(cronografo), NULL);
	tid_tiempo_LCD = osThreadCreate (osThread(tiempo_LCD), NULL);
	tid_thread_papadeo = osThreadCreate (osThread(thread_papadeo), NULL);
  if (!tid_cronografo | !tid_tiempo_LCD | !timer_1seg_id | !tid_thread_papadeo) return(-1);
  
  return(0);
}


void cronografo (void const *argument) {
	
	//uint32_t delay = 1;
	
	while (1) {
		osDelay(1000);
//		osTimerStart(timer_1seg_id,1000);
		
	  osSignalSet(tid_tiempo_LCD,0x01);		
		
		osThreadYield ();       
		
  }
}

void tiempo_LCD (void const *argument){
	
	int seg = 0;
	int min = 2;
	
	osEvent signals;
	
	LCD_reset();
	
	while(1){	
		
		signals = osSignalWait(0x00,osWaitForever);
		
		if (signals.value.signals == 0x01){
		
			limpiaBuffer();
			
			imprimeLCD("Crono: ",1);
			if (min < 10) escribirNumeroEntero(0,1);
			escribirNumeroEntero(min,1);
			imprimeLCD(":",1);
			if (seg < 10) escribirNumeroEntero(0,1);
			escribirNumeroEntero(seg,1);
			copy_to_lcd(1);
			
			if (min == 1 && seg%4 == 0)
				osSignalSet(tid_thread_papadeo,0x01);
			
			if (min == 0 &&  seg == 0)
				osSignalSet(tid_thread_papadeo,0x02);
			
//			if (min == 0 && seg == 0){
//					min = 2;
//			}	
	
			if(seg == 0 && min > 0){
					min--;
					seg = 59;
			}
			if (seg < 60 && seg > 0){
					seg--;
			}else if (seg == 0 && min == 0){
					seg = 0;
					min = 0;
			}
			
			osSignalClear(tid_tiempo_LCD,0x01);
		}
		
		osThreadYield ();
	}
}


void thread_papadeo(void const *argument){
	
	osEvent signals;
	static char valor = 0;
	
	while(1){
		signals = osSignalWait(0x00,osWaitForever);
		
		if (signals.value.signals == 0x01){
			for (int i = 15 ; i >= 0 ; i--){
				valor ^= 0x01;			
				GPIO_PinWrite(PUERTO_LED,LED_1,valor);
				osDelay(125);
			}
		}
			
		if (signals.value.signals == 0x02){
			GPIO_PinWrite(PUERTO_LED,LED_1,1);
			osDelay(125);
			GPIO_PinWrite(PUERTO_LED,LED_1,0);
			osDelay(125);
			GPIO_PinWrite(PUERTO_LED,LED_2,1);
			osDelay(125);
			GPIO_PinWrite(PUERTO_LED,LED_2,0);
			osDelay(125);
			GPIO_PinWrite(PUERTO_LED,LED_3,1);
			osDelay(125);
			GPIO_PinWrite(PUERTO_LED,LED_3,0);
			osDelay(125);
			GPIO_PinWrite(PUERTO_LED,LED_4,1);
			osDelay(125);
			GPIO_PinWrite(PUERTO_LED,LED_4,0);
			osDelay(125);
			GPIO_PinWrite(PUERTO_LED,LED_4,1);
			osDelay(125);
			GPIO_PinWrite(PUERTO_LED,LED_4,0);
			osDelay(125);
			GPIO_PinWrite(PUERTO_LED,LED_3,1);
			osDelay(125);
			GPIO_PinWrite(PUERTO_LED,LED_3,0);
			osDelay(125);
			GPIO_PinWrite(PUERTO_LED,LED_2,1);
			osDelay(125);
			GPIO_PinWrite(PUERTO_LED,LED_2,0);
			osDelay(125);
			GPIO_PinWrite(PUERTO_LED,LED_1,1);
			osDelay(125);
			GPIO_PinWrite(PUERTO_LED,LED_1,0);
			osDelay(125);
		}
		osThreadYield ();
	}
	
}


void  timer1_seg_Callback(void const *argument){
		osSignalSet(tid_tiempo_LCD,0x01);
}

void  parpadeo_Callback(void const *argument){
	
}
