#include "LPC17xx.h"
#include "PIN_LPC17xx.h"
#include "GPIO_LPC17xx.h"
#include "Driver_SPI.h"
#include "GPDMA_LPC17xx.h"


// RETRASA LOS MICROSEGUNDOS PASADOS POR PARAMETRO
// DE ENTRADA, CON CORRECCION DE RETRASO SOFTWARE/HARDWARE
extern void retraso (uint32_t microsegundos);

//CONFIGURA E INICIALIZA EL LCD
extern void init_SPI(void);

// ESCRITURA DE DATOS
extern void wr_data(unsigned char data);

// ESCRITURA DE COMANDO
extern void wr_cmd(unsigned char cmd);
	
// HACE RESET DEL LCD
extern void LCD_reset(void);

// COPIA EN EL LCD LO ALMACENADO EN EL BUFFER
extern void copy_to_lcd(int pagina);

// GENERA LA LETRA A
extern void letraA(unsigned char buf[]);

// ESCRIBE UN CARACTER EN LA LINEA 2 DEL LCD
extern int escribeLetra_L1(uint8_t letra);

// ESCRIBE UN CARACTER EN LA LINEA 2 DEL LCD
extern int escribeLetra_L2(uint8_t letra);

//EXCRIBE UN NUMERO ENTERO DE 32 BITS SIN SIGNO (numero) EN LA LINEA SELECCIONADA (linea)
extern void escribirNumeroEntero(uint32_t numero, int linea);

//EXCRIBE UN NUMERO DECIMAL(numero) EN LA LINEA SELECCIONADA (linea)
extern void escribirNumeroDecimal(int numero, int linea);

//IMPRIME POR PANTALLA DE LCD UN STRING COMPLETO (cadena) EN LA LINEA SELECCIONADA(linea)
void imprimeLCD(char cadena[], int linea);

//LIMPIA EL BUFFER 
void limpiaBuffer(void);

