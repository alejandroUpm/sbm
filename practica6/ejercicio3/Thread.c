
#include "cmsis_os.h"                                           // CMSIS RTOS header file
#include "LCD.h"
#include "utils.h"
#include "stdio.h"
#include "constants.h"



/*----------------------------------------------------------------------------
 *      Thread 1 'Thread_Name': Sample thread
 *---------------------------------------------------------------------------*/
 
 // THREADS
void cronografo (void const *argument);                             
osThreadId tid_cronografo;                                          
osThreadDef (cronografo, osPriorityNormal, 1, 0);                   

void tiempo_LCD (void const *argument);
osThreadId tid_tiempo_LCD;
osThreadDef (tiempo_LCD, osPriorityNormal, 1, 0);

void thread_papadeo(void const *argument);
osThreadId tid_thread_papadeo;
osThreadDef (thread_papadeo, osPriorityNormal, 1, 0);

// VIRTUAL TIMERS
void  timer1_seg_Callback(void const *argument);
osTimerDef(timer1_seg,timer1_seg_Callback);
osTimerId timer_1seg_id;


int Init_Thread (void) {
	//timers
	timer_1seg_id = osTimerCreate(osTimer(timer1_seg),osTimerPeriodic ,NULL);
	
	//threads
  tid_cronografo = osThreadCreate (osThread(cronografo), NULL);
	tid_tiempo_LCD = osThreadCreate (osThread(tiempo_LCD), NULL);
	tid_thread_papadeo = osThreadCreate (osThread(thread_papadeo), NULL);
  if (!tid_cronografo | !tid_tiempo_LCD | !timer_1seg_id | !tid_thread_papadeo) return(-1);
  
  return(0);
}


void cronografo (void const *argument) {

		osTimerStart(timer_1seg_id,1000);
		osThreadYield ();       
}

void tiempo_LCD (void const *argument){
	
	int seg = 0;
	int min = 2;
	
	static int pause = 0;
	
	osEvent signals;
	
	LCD_reset();
	
	while(1){	
		
		signals = osSignalWait(0x00,osWaitForever);
		
		// INTERRUPTION JS LEFT, RESETS THE COUNT
		if(signals.value.signals == 0x10){
				seg = 0;
				min = 2;
			}
		
		// INTERRUPTION JS CENTER, PAUSES THE COUNT 
		if(signals.value.signals == 0x20)
			 pause ^= 0x01;
		
		// INTERRUPTION JS UP, INCREASES THE COUNT BY 10  
		if(signals.value.signals == 0x40){
				
			seg = seg + 10;
			
			if(seg >= 60){
				seg = seg - 60;
				min++;
			}
			
			if(min > 1){
				min = 2;
				seg = 0;
			}
		}
		
		// INTERRUPTION JS DOWN, DECREASES THE COUNT BY 10
		if(signals.value.signals == 0x80){
			
			if(min == 0 && seg < 10){
				seg = 0;
			}else if(seg < 10){
				seg = seg + 50;
				min--;
			}
			else{
				seg = seg - 10;
			}
		}
				
		if (signals.value.signals == 0x01){
								
			limpiaBuffer();
			
			imprimeLCD("Crono: ",1);
			if (min < 10) escribirNumeroEntero(0,1);
			escribirNumeroEntero(min,1);
			imprimeLCD(":",1);
			if (seg < 10) escribirNumeroEntero(0,1);
			escribirNumeroEntero(seg,1);
			copy_to_lcd(1);
			
			if (min == 1 && seg%4 == 0)
				osSignalSet(tid_thread_papadeo,0x01);
			
			if (min == 0 &&  seg == 0)
				osSignalSet(tid_thread_papadeo,0x02);
			
			if (pause == 0){
				if (seg < 60 && seg > 0){
						seg--;
				}else if(seg == 0 && min > 0){
						min--;
						seg = 59;
				}else if (seg == 0 && min == 0){
						seg = 0;
						min = 0;
				}		
			}
			
			osSignalClear(tid_tiempo_LCD,0x01);
		}
		
		osThreadYield ();
	}
}


void thread_papadeo(void const *argument){
	
	osEvent signals;
	static char valor = 1;
	
	while(1){
		signals = osSignalWait(0x00,osWaitForever);
		
		if (signals.value.signals == 0x01){
			
			for (int i = 15 ; i >= 0 ; i--){			
				GPIO_PinWrite(PUERTO_LED,LED_1,valor);
				valor ^= 0x01;
				osDelay(125);
			}
			GPIO_PinWrite(PUERTO_LED,LED_1,0);
		}
			
		if (signals.value.signals == 0x02){
			GPIO_PinWrite(PUERTO_LED,LED_1,1);
			osDelay(125);
			GPIO_PinWrite(PUERTO_LED,LED_1,0);
			osDelay(125);
			GPIO_PinWrite(PUERTO_LED,LED_2,1);
			osDelay(125);
			GPIO_PinWrite(PUERTO_LED,LED_2,0);
			osDelay(125);
			GPIO_PinWrite(PUERTO_LED,LED_3,1);
			osDelay(125);
			GPIO_PinWrite(PUERTO_LED,LED_3,0);
			osDelay(125);
			GPIO_PinWrite(PUERTO_LED,LED_4,1);
			osDelay(125);
			GPIO_PinWrite(PUERTO_LED,LED_4,0);
			osDelay(125);
			GPIO_PinWrite(PUERTO_LED,LED_4,1);
			osDelay(125);
			GPIO_PinWrite(PUERTO_LED,LED_4,0);
			osDelay(125);
			GPIO_PinWrite(PUERTO_LED,LED_3,1);
			osDelay(125);
			GPIO_PinWrite(PUERTO_LED,LED_3,0);
			osDelay(125);
			GPIO_PinWrite(PUERTO_LED,LED_2,1);
			osDelay(125);
			GPIO_PinWrite(PUERTO_LED,LED_2,0);
			osDelay(125);
			GPIO_PinWrite(PUERTO_LED,LED_1,1);
			osDelay(125);
			GPIO_PinWrite(PUERTO_LED,LED_1,0);
			osDelay(125);
		}
		osThreadYield ();
	}
	
}


void  timer1_seg_Callback(void const *argument){
		osSignalSet(tid_tiempo_LCD,0x01);
}

