
#include "cmsis_os.h"   // CMSIS RTOS header file
#include "GPIO_LPC17xx.h"
#include "LPC17xx.h"
#include "constants.h"
#include "utils.h"

extern int Shift;
extern int velocidad;

/*----------------------------------------------------------------------------
 *      Thread 1 'Thread_Name': Sample thread
 *---------------------------------------------------------------------------*/
 
void antiRebotes (void const *argument);                             // thread function
osThreadId tid_antiRebotes;                                          // thread id
osThreadDef (antiRebotes, osPriorityNormal, 1, 0);                   // thread object

void leds (void const *argument);
osThreadId tid_leds;
osThreadDef (leds, osPriorityNormal, 1, 0);

int Init_Thread (void) {

  tid_antiRebotes = osThreadCreate (osThread(antiRebotes), NULL);
  tid_leds = osThreadCreate (osThread(leds), NULL);
  if (!tid_antiRebotes || !tid_leds) return(-1);
  
  return(0);
}

void antiRebotes (void const *argument) {

	osEvent signals;
	
  while (1) {
    signals = osSignalWait(0x00,osWaitForever);
		
		if (signals.value.signals == 0x01){	
      Shift = 1; 
    }
    
    if (signals.value.signals == 0x02){		
      Shift = 0; 
    }
    
    if (signals.value.signals == 0x04){		
       if (velocidad < 5) 
       {
          velocidad++;
       }
       else
       {
          velocidad = 5;
       }
    }
    
    if (signals.value.signals == 0x08){		
      if (velocidad > 1)
      {
          velocidad--;
      }
      else
      {
          velocidad = 1;
      }
    }
    
    if (signals.value.signals == 0x03)		// FALLING EDGE BOUNCE
    {
        NVIC_EnableIRQ(EINT3_IRQn);
    }
    
    osSignalClear(tid_antiRebotes,0x01);
    osThreadYield ();                                           // suspend thread
  }
}


void leds (void const *argument){
	
  while(1){
		if (Shift == 0)
		{
			GPIO_PinWrite(PUERTO_LED,LED_1,1);
			osDelay(200/velocidad);
			GPIO_PinWrite(PUERTO_LED,LED_1,0);
			osDelay(200/velocidad);
			GPIO_PinWrite(PUERTO_LED,LED_2,1);
			osDelay(200/velocidad);
			GPIO_PinWrite(PUERTO_LED,LED_2,0);
			osDelay(200/velocidad);
			GPIO_PinWrite(PUERTO_LED,LED_3,1);
			osDelay(200/velocidad);
			GPIO_PinWrite(PUERTO_LED,LED_3,0);
			osDelay(200/velocidad);
			GPIO_PinWrite(PUERTO_LED,LED_4,1);
			osDelay(200/velocidad);
			GPIO_PinWrite(PUERTO_LED,LED_4,0);
			osDelay(200/velocidad);
				
		}
		else
		{
			GPIO_PinWrite(PUERTO_LED,LED_4,1);
			osDelay(200/velocidad);
			GPIO_PinWrite(PUERTO_LED,LED_4,0);
			osDelay(200/velocidad);
			GPIO_PinWrite(PUERTO_LED,LED_3,1);
			osDelay(200/velocidad);
			GPIO_PinWrite(PUERTO_LED,LED_3,0);
			osDelay(200/velocidad);
			GPIO_PinWrite(PUERTO_LED,LED_2,1);
			osDelay(200/velocidad);
			GPIO_PinWrite(PUERTO_LED,LED_2,0);
			osDelay(200/velocidad);
			GPIO_PinWrite(PUERTO_LED,LED_1,1);
			osDelay(200/velocidad);
			GPIO_PinWrite(PUERTO_LED,LED_1,0);
			osDelay(200/velocidad);
		
		}
    osThreadYield (); 
  }
    
}

