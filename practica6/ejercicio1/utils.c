
#include "constants.h"
#include "LPC17xx.h"
#include "GPIO_LPC17xx.h"
#include "PIN_LPC17xx.h"
#include "cmsis_os.h" 
#include "utils.h"

int velocidad = 1;
int Shift = 0;

extern osThreadId tid_antiRebotes;

void InizialisePeripherals(void){
		
	//inicilization for the leds
	GPIO_SetDir(PUERTO_LED,LED_1,GPIO_DIR_OUTPUT);	
	GPIO_SetDir(PUERTO_LED,LED_2,GPIO_DIR_OUTPUT);
	GPIO_SetDir(PUERTO_LED,LED_3,GPIO_DIR_OUTPUT);
	GPIO_SetDir(PUERTO_LED,LED_4,GPIO_DIR_OUTPUT);
	
	//inicialization of the JS
	PIN_Configure(PUERTO_JS,RIGHT,PIN_FUNC_0,PIN_PINMODE_PULLDOWN,PIN_PINMODE_NORMAL);
	PIN_Configure(PUERTO_JS,DOWN,PIN_FUNC_0,PIN_PINMODE_PULLDOWN,PIN_PINMODE_NORMAL);
	PIN_Configure(PUERTO_JS,UP,PIN_FUNC_0,PIN_PINMODE_PULLDOWN,PIN_PINMODE_NORMAL);
	PIN_Configure(PUERTO_JS,LEFT,PIN_FUNC_0,PIN_PINMODE_PULLDOWN,PIN_PINMODE_NORMAL);
	
	//clear the flags on the first place (to make sure)
	LPC_GPIOINT->IO0IntClr |= 1 << LEFT;
	LPC_GPIOINT->IO0IntClr |= 1 << DOWN;
	LPC_GPIOINT->IO0IntClr |= 1 << UP;
	LPC_GPIOINT->IO0IntClr |= 1 << RIGHT;

	// enables the interruption on the fall
	LPC_GPIOINT->IO0IntEnF |= 1 << RIGHT; 
	LPC_GPIOINT->IO0IntEnF |= 1 << DOWN; 
	LPC_GPIOINT->IO0IntEnF |= 1 << UP; 
	LPC_GPIOINT->IO0IntEnF |= 1 << LEFT; 
	
	NVIC_EnableIRQ(EINT3_IRQn);
			
}

void EINT3_IRQHandler(void)
{
  if (LPC_GPIOINT->IO0IntStatF & (1 << LEFT)){
	osDelay(50);
    osSignalSet(tid_antiRebotes,0x01);
    LPC_GPIOINT->IO0IntClr |= 1 << LEFT;
  }
  
  if(LPC_GPIOINT->IO0IntStatF & (1 << RIGHT)){
	osDelay(50);
    osSignalSet(tid_antiRebotes,0x02);
    LPC_GPIOINT->IO0IntClr |= 1 << RIGHT;    
  }
  
  if(LPC_GPIOINT->IO0IntStatF & (1 << UP)){
    osDelay(50);
	osSignalSet(tid_antiRebotes,0x04);
    LPC_GPIOINT->IO0IntClr |= 1 << UP;    
  }
  
  if(LPC_GPIOINT->IO0IntStatF & (1 << DOWN)){
    osDelay(50);
	osSignalSet(tid_antiRebotes,0x08);
    LPC_GPIOINT->IO0IntClr |= 1 << DOWN;    
  } 
  
  if (LPC_GPIOINT->IO0IntStatR & (1 << DOWN) || LPC_GPIOINT->IO0IntStatR & (1 << UP) ||
      LPC_GPIOINT->IO0IntStatR & (1 << LEFT) || LPC_GPIOINT->IO0IntStatR & (1 << RIGHT)){
    osDelay(50);
	osSignalSet(tid_antiRebotes,0x03);
    LPC_GPIOINT->IO0IntClr |= 1 << LEFT;
		LPC_GPIOINT->IO0IntClr |= 1 << DOWN;
		LPC_GPIOINT->IO0IntClr |= 1 << UP;
		LPC_GPIOINT->IO0IntClr |= 1 << RIGHT;
  }
  
}

