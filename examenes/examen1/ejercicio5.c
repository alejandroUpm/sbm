#include "GPIO_LPC17xx.h"
#include "PIN_LPC17xx.h"
#include "LPC17xx.h"


#define Puerto2 2
#define RED     3
#define GREEN   2
#define BLUE    1

#define  un_ms  (uint32_t) 12500 

void configPines(void){
  
  //PIN_Configure(Puerto2,RED,PIN_FUNC_1,PIN_PINMODE_PULLDOWN,PIN_PINMODE_NORMAL);
  PIN_Configure(Puerto2,GREEN,PIN_FUNC_1,PIN_PINMODE_PULLDOWN,PIN_PINMODE_NORMAL);
  //PIN_Configure(Puerto2,BLUE,PIN_FUNC_1,PIN_PINMODE_PULLDOWN,PIN_PINMODE_NORMAL);
  
  //GPIO_SetDir(Puerto2,RED,GPIO_DIR_OUTPUT);
  GPIO_SetDir(Puerto2,GREEN,GPIO_DIR_OUTPUT);
  //GPIO_SetDir(Puerto2,BLUE,GPIO_DIR_OUTPUT);

}

void configPWM(void){


  LPC_SC->PCONP|=(1<<6); //Activar alimentacion en perifericos
  
  LPC_SC->PCLKSEL0 |=((1<<13)|(1<<12));//Reloj cclk/8
  
  LPC_PWM1->MR0 = 12500;//Periodo de cuenta total
  LPC_PWM1->MR2 = 12500/2;//Mitad de cuenta (ciclo 50%) BLUE
  LPC_PWM1->MR3 = 12500/2;//Mitad de cuenta (ciclo 50%)GREEN
  LPC_PWM1->MR4 = 12500/2;//Mitad de cuenta (ciclo 50%) RED
  
  
  //b0 habilita cuenta, b1 pone el reset, b3 activa modo PWM)
  LPC_PWM1->TCR |= ((1 << 3)|(1 << 1)|(1 << 0));
  
  LPC_PWM1->MCR |= (1 << 1); //Reset despues de alcanzar la cuenta canal 0
  
  LPC_PWM1->PCR |= (1 << 12); //Habilitar la salida PWM1.4 RED
  LPC_PWM1->PCR |= (1 << 11); //Habilitar la salida PWM1.3 GREEN
  LPC_PWM1->PCR |= (1 << 10); //Habilitar la salida PWM1.2 BLUE
  
  LPC_PWM1->TCR &= 0xFFFFFFFD; //Quita el reset para permitir la cuenta
}

void cambiarPWM(uint32_t ciclo){
  
 uint32_t cuenta = 0;
  
 // b1 pone el reset
  LPC_PWM1->TCR |= (1 << 1);
 
//Calcular el numero de cuentas en funcion del ciclo  
 cuenta = (un_ms*ciclo/100);
  
  //NOTA: PARA ACTUALIZAR CON NUEVOS VALORES LOS REGITROS MRx ES NECESARIO
  //PONER LOS BITS CORRSPONDIENTES A 1 EN EL REGISTRO "LER"
  
 //Nuevos valores de ciclo de trabajo 
 LPC_PWM1->MR2 = cuenta/4;  // cuenta BLUE
 LPC_PWM1->MR3 = cuenta;    // cuenta GREEN
 LPC_PWM1->MR4 = cuenta/2;  // cuenta RED
  
 //Registro LER habilita la carga de los nuevos valores (MR2,MR3,MR4)
 LPC_PWM1->LER |= ((1 << 2)|(1 << 3)|(1 << 4));
  
 //Quita el reset para permitir la cuenta
 LPC_PWM1->TCR &= 0xFFFFFFFD; 
  for(int j = 0; j < 3000000 ; j++);
}

void SysTick_Handler(){
	static uint32_t i = 5;
	for(i = 5;i < 100; i = i + 5)
	{
		cambiarPWM(i); 
		if (i >= 100)
		{
			i = 0;
		}
  }
}

int main (void){
    
  configPines();
  configPWM();
 
	SystemCoreClockUpdate();
	SysTick_Config(SystemCoreClock/100);
		
	while(1);

}
