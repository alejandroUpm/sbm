
#include "LPC17xx.h"
#include "GPIO_LPC17xx.h"
#include "PIN_LPC17xx.h"
#include "constants.h"


//ctes timer0
#define FREQ_TIMER0_100		2
#define PIN_TIMER0				29
#define PUERTO_TIMER0			1

//ctes timer2
#define FREQ_TIMER2_100		12
#define TIMER2_ENABLE			22
#define PUERTO_TIMER2			0
#define PIN_TIMER2				6


//ctes timer3
#define FREQ_TIMER3_100		14
#define TIMER3_ENABLE			23
#define PUERTO_TIMER3			0
#define PIN_TIMER3				10


uint32_t Shift = 0;
int velocidad = 1;

//flags para el manejo del control de rebotes
bool DflagLeft = 0;
bool DflagRight = 0;
bool DflagUp = 0;
bool DflagDown = 0;
  
bool Uflag = 0;
  
int status = 0;

void TIMER2_IRQHandler(void)
{
  if (DflagLeft)
  {
      Shift = 1;  
      DflagLeft = 0;  
			status -= 1;
  }

	if (DflagRight)
  {
      Shift = 0; 
      DflagRight = 0;
			status += 1;
  }
  
  if (DflagUp)
  {
     if (velocidad < 5) 
     {
        velocidad++;
     }
		 else
		 {
				velocidad = 5;
		 }
     DflagUp = 0;
  }
  
  if (DflagDown)
	{
		if (velocidad > 1)
		{
				velocidad--;
		}
		else
		{
				velocidad = 1;
		}
    DflagDown = 0;
	}
  NVIC_EnableIRQ(EINT3_IRQn);
  SysTick_Config((SystemCoreClock/(100*velocidad)));
  LPC_TIM2->IR |= 0x00000001;
  
}

void TIMER3_IRQHandler(void)
{
  if (Uflag)
  {
    NVIC_EnableIRQ(EINT3_IRQn);
    Uflag = 0;
  }
  LPC_TIM3->IR |= 0x00000001;
}


void EINT3_IRQHandler(void)
{
 
	if (LPC_GPIOINT->IO0IntStatF & (1 << LEFT))
	{
		LPC_TIM0->TC += 0x00000001;
    LPC_TIM2->TCR |= 0x2;
		LPC_TIM2->TCR = 0x1;
    DflagLeft = 1;
    NVIC_DisableIRQ(EINT3_IRQn);
	}
	
	if(LPC_GPIOINT->IO0IntStatF & (1 << RIGHT))
	{
		LPC_TIM0->TC += 0x00000001;
		LPC_TIM2->TCR |= 0x2;
		LPC_TIM2->TCR = 0x1;
    DflagRight = 1;
    NVIC_DisableIRQ(EINT3_IRQn);
		
	}
	
	if(LPC_GPIOINT->IO0IntStatF & (1 << UP))
	{
		LPC_TIM0->TC += 0x00000001;
		LPC_TIM2->TCR |= 0x2;
		LPC_TIM2->TCR = 0x1;
    DflagUp = 1;
    NVIC_DisableIRQ(EINT3_IRQn);
	}
	
	if(LPC_GPIOINT->IO0IntStatF & (1 << DOWN))
	{
		LPC_TIM0->TC += 0x00000001;
		LPC_TIM2->TCR |= 0x2;
		LPC_TIM2->TCR = 0x1;
    DflagDown = 1;
    NVIC_DisableIRQ(EINT3_IRQn);
	}
  
  if (LPC_GPIOINT->IO0IntStatR & (1 << DOWN) || LPC_GPIOINT->IO0IntStatR & (1 << UP) ||
      LPC_GPIOINT->IO0IntStatR & (1 << LEFT) || LPC_GPIOINT->IO0IntStatR & (1 << RIGHT))
  {
    LPC_TIM3->TCR |= 0x2;
    LPC_TIM3->TCR = 0x1;
    Uflag = 1;
    NVIC_DisableIRQ(EINT3_IRQn);
  }
	
	LPC_GPIOINT->IO0IntClr |= 1 << LEFT;
	LPC_GPIOINT->IO0IntClr |= 1 << DOWN;
	LPC_GPIOINT->IO0IntClr |= 1 << UP;
	LPC_GPIOINT->IO0IntClr |= 1 << RIGHT;
}

////		NUEVA FUNCIONALIDAD
//void SysTick_Handler(void)
//{	
//		if (status >= 0 && status < 4)
//		{
//			switch(status)
//			{
//				case 0: 
//					GPIO_PinWrite(PUERTO_LED,LED_1,1);
//					GPIO_PinWrite(PUERTO_LED,LED_2,0);
//				  GPIO_PinWrite(PUERTO_LED,LED_3,0);
//					GPIO_PinWrite(PUERTO_LED,LED_4,0);
//				break;
//				case 1: 
//					GPIO_PinWrite(PUERTO_LED,LED_1,0);
//					GPIO_PinWrite(PUERTO_LED,LED_2,1);
//				  GPIO_PinWrite(PUERTO_LED,LED_3,0);
//					GPIO_PinWrite(PUERTO_LED,LED_4,0);				
//				break;
//				case 2: 
//					GPIO_PinWrite(PUERTO_LED,LED_1,0);
//					GPIO_PinWrite(PUERTO_LED,LED_2,0);
//				  GPIO_PinWrite(PUERTO_LED,LED_3,1);
//					GPIO_PinWrite(PUERTO_LED,LED_4,0);				
//				break;
//				case 3: 
//					GPIO_PinWrite(PUERTO_LED,LED_1,0);
//					GPIO_PinWrite(PUERTO_LED,LED_2,0);
//				  GPIO_PinWrite(PUERTO_LED,LED_3,0);
//					GPIO_PinWrite(PUERTO_LED,LED_4,1);				
//				break;
//			}
//		}
//		else if (status > 3) 
//		{
//			status = 0;
//		}
//		else if(status < 0)
//		{
//			status = 3;
//		}
//	}	


// 		PROGRAMA ORIGINAL PRACTICA 2 EJERCICIO 4
void SysTick_Handler(void)
{
  static uint32_t ticks;
	
		if (Shift == 0)
		{
			switch(ticks++)
			{
				case 0: GPIO_PinWrite(PUERTO_LED,LED_1,1);break;
				case 5: GPIO_PinWrite(PUERTO_LED,LED_1,0);break;
				case 10: GPIO_PinWrite(PUERTO_LED,LED_2,1);break;
				case 15: GPIO_PinWrite(PUERTO_LED,LED_2,0);break;
				case 20: GPIO_PinWrite(PUERTO_LED,LED_3,1);break;
				case 25: GPIO_PinWrite(PUERTO_LED,LED_3,0);break;
				case 30: GPIO_PinWrite(PUERTO_LED,LED_4,1);break;
				case 35: GPIO_PinWrite(PUERTO_LED,LED_4,0);break;
				default:
					if (ticks > 36)
					{
						ticks = 0;
					}
			}
		}
		else
		{
			switch(ticks++)
			{
				case 0: GPIO_PinWrite(PUERTO_LED,LED_4,1);break;
				case 5: GPIO_PinWrite(PUERTO_LED,LED_4,0);break;
				case 10: GPIO_PinWrite(PUERTO_LED,LED_3,1);break;
				case 15: GPIO_PinWrite(PUERTO_LED,LED_3,0);break;
				case 20: GPIO_PinWrite(PUERTO_LED,LED_2,1);break;
				case 25: GPIO_PinWrite(PUERTO_LED,LED_2,0);break;
				case 30: GPIO_PinWrite(PUERTO_LED,LED_1,1);break;
				case 35: GPIO_PinWrite(PUERTO_LED,LED_1,0);break;
				default:
					if (ticks > 36)
					{
						ticks = 0;
					}
			}
		}
}

int main ()
{
	// CONFIGURACION DEL TIMER 0
	//habilitacion del contador haciendo uso del timer 2
	LPC_SC->PCLKSEL0 |= 1 << FREQ_TIMER0_100;
	//configuracion del timer al pin fisico 8
	GPIO_SetDir(PUERTO_TIMER0,PIN_TIMER0,GPIO_DIR_OUTPUT);
	PIN_Configure(PUERTO_TIMER0,PIN_TIMER0,PIN_FUNC_3,PIN_PINMODE_PULLUP,PIN_PINMODE_NORMAL);
	// habilita la cuenta del contador
	LPC_TIM0->TCR |= 0x1;
	//establece modo contador (no timer) para el cap2.0 para timer2
	LPC_TIM0->CTCR = 0x1;	
	
	// CONFIGURACION DEL TIMER 2
	LPC_SC->PCONP |= 1 << TIMER2_ENABLE;
	LPC_SC->PCLKSEL1 |= 1 << FREQ_TIMER2_100;
	GPIO_SetDir(PUERTO_TIMER2,PIN_TIMER2,GPIO_DIR_OUTPUT);
	PIN_Configure(PUERTO_TIMER2,PIN_TIMER2,PIN_FUNC_3,PIN_PINMODE_PULLUP,PIN_PINMODE_NORMAL);
	//configuracion del timer 2 a nivel de registro
	LPC_TIM2->IR |= 0x00000001;
	LPC_TIM2->MCR  |=  0x001;
	LPC_TIM2->MR0 |= 0x00989680;
	
	// CONFIGURACION DEL TIMER 3
	LPC_SC->PCONP |= 1 << TIMER3_ENABLE;
	LPC_SC->PCLKSEL1 |= 1 << FREQ_TIMER3_100;
	GPIO_SetDir(PUERTO_TIMER3,PIN_TIMER3,GPIO_DIR_OUTPUT);
	PIN_Configure(PUERTO_TIMER3,PIN_TIMER3,PIN_FUNC_3,PIN_PINMODE_PULLUP,PIN_PINMODE_NORMAL);
	//configuracion del timer 3 a nivel de registro
	LPC_TIM3->IR |= 0x00000001;
	LPC_TIM3->MCR  |=  0x001;
	LPC_TIM3->MR0 |= 0x01989680;	// 100 ms = 0x00989680
	
	// configuracion direccion y tipo de pin para los LED
  GPIO_SetDir(PUERTO_LED,LED_1,GPIO_DIR_OUTPUT);	
	GPIO_SetDir(PUERTO_LED,LED_2,GPIO_DIR_OUTPUT);
	GPIO_SetDir(PUERTO_LED,LED_3,GPIO_DIR_OUTPUT);
	GPIO_SetDir(PUERTO_LED,LED_4,GPIO_DIR_OUTPUT);
	
	PIN_Configure(PUERTO_JS,RIGHT,PIN_FUNC_0,PIN_PINMODE_PULLDOWN,PIN_PINMODE_NORMAL);
	PIN_Configure(PUERTO_JS,DOWN,PIN_FUNC_0,PIN_PINMODE_PULLDOWN,PIN_PINMODE_NORMAL);
	PIN_Configure(PUERTO_JS,UP,PIN_FUNC_0,PIN_PINMODE_PULLDOWN,PIN_PINMODE_NORMAL);
	PIN_Configure(PUERTO_JS,LEFT,PIN_FUNC_0,PIN_PINMODE_PULLDOWN,PIN_PINMODE_NORMAL);
	
  LPC_GPIOINT->IO0IntClr |= 1 << LEFT;
	LPC_GPIOINT->IO0IntClr |= 1 << DOWN;
	LPC_GPIOINT->IO0IntClr |= 1 << UP;
	LPC_GPIOINT->IO0IntClr |= 1 << RIGHT;

	LPC_GPIOINT->IO0IntEnF |= 1 << RIGHT; 
	LPC_GPIOINT->IO0IntEnF |= 1 << DOWN; 
	LPC_GPIOINT->IO0IntEnF |= 1 << UP; 
	LPC_GPIOINT->IO0IntEnF |= 1 << LEFT; 
	
	NVIC_EnableIRQ(EINT3_IRQn);
	NVIC_EnableIRQ(TIMER2_IRQn);
	NVIC_EnableIRQ(TIMER3_IRQn);
	
  SystemCoreClockUpdate();												// funcion para actualizar el reloj 
  SysTick_Config((SystemCoreClock/100));					//frecuencia de 20 milisegundos cada tick, 
																									// multiplicado por el coeficiente velocidad
																									// manejado por la interrupcion
	while(1);
}










