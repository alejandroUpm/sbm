
#include "cmsis_os.h"                                           // CMSIS RTOS header file
#include "constants.h"
#include "LCD.h"
#include "utils.h"
#include "stdio.h"
#include "constants.h"

/*----------------------------------------------------------------------------
 *      Thread 1 'Thread_Name': Sample thread
 *---------------------------------------------------------------------------*/
 
void LCD (void const *argument);                             // thread function
osThreadId tid_Thread;                                          // thread id
osThreadDef (LCD, osPriorityNormal, 1, 0);                   // thread object

int contador = 0;
int contador2 = 0;
int linea = 0;

int Init_Thread (void) {

  tid_Thread = osThreadCreate (osThread(LCD), NULL);
  if (!tid_Thread) return(-1);
  
  return(0);
}

void LCD (void const *argument) {

	osEvent signals;
	
	limpiaBuffer();
	
  while (1) {
    
		signals = osSignalWait(0x00,4000);
		
		if(signals.value.signals == 0x80){
			linea = 1;
		}
		
		if(signals.value.signals == 0x40){
			linea = 0;
		}
		
		if(signals.status == osEventTimeout){
			GPIO_PinWrite(PUERTO_LED,LED_2,1);
			osDelay(100);
			GPIO_PinWrite(PUERTO_LED,LED_2,0);			
		}
		if(signals.value.signals == 0x10){
			GPIO_PinWrite(PUERTO_LED,LED_1,0);
			if(contador > 0 && linea == 0){
				menosLinea(contador);
				contador--;
			}else if(contador2 < 128 && linea == 1){
				menosLinea2(contador2);
				contador2--;
			}
		}
		if(signals.value.signals == 0x01){
			GPIO_PinWrite(PUERTO_LED,LED_1,0);
			if(contador < 127 && linea == 0){
				masLinea(contador);
				contador++;
			}else if(contador2 < 127 && linea == 1){
				masLinea2(contador2);
				contador2++;
			}
		}
				
		copy_to_lcd(0);
		copy_to_lcd(1);
    osThreadYield ();                                           // suspend thread
  }
}


	


