
#include "cmsis_os.h"                                           // CMSIS RTOS header file
#include "constants.h"
#include "LCD.h"
#include "utils.h"
#include "stdio.h"
#include "constants.h"

/*----------------------------------------------------------------------------
 *      Thread 1 'Thread_Name': Sample thread
 *---------------------------------------------------------------------------*/
 
void LCD (void const *argument);                             // thread function
osThreadId tid_Thread;                                          // thread id
osThreadDef (LCD, osPriorityNormal, 1, 0); 

void timer (void const *argument);                             // thread function
osThreadId tid_Thread2;                                          // thread id
osThreadDef (timer, osPriorityNormal, 1, 0); 

void  timer_seg_Callback(void const *argument);
osTimerDef(timer_seg,timer_seg_Callback);
osTimerId timer_seg_id;

int contador = 0;

int Init_Thread (void) {
	
	timer_seg_id = osTimerCreate(osTimer(timer_seg),osTimerOnce ,NULL);

  tid_Thread = osThreadCreate (osThread(LCD), NULL);
	tid_Thread2 = osThreadCreate (osThread(timer), NULL);
  if (!tid_Thread) return(-1);
  
  return(0);
}

void timer (void const *argument){
	osEvent signals;
	
	while(1){
		
		signals = osSignalWait(0x00,10000);
		
		if(signals.status == osEventTimeout){
			GPIO_PinWrite(PUERTO_LED,LED_2,1);
		}
		
		osThreadYield ();  
	}
}


void LCD (void const *argument) {

	osEvent signals;
	
	
  while (1) {
    
		limpiaBuffer();
		
		switch(contador){
			case 0:	imprimeLCD("A________________",0);break;
			case 1:	imprimeLCD("_A_______________",0);break;
			case 2:	imprimeLCD("__A______________",0);break;
			case 3:	imprimeLCD("___A_____________",0);break;
			case 4:	imprimeLCD("____A____________",0);break;
			case 5:	imprimeLCD("_____A___________",0);break;
			case 6:	imprimeLCD("______A__________",0);break;
			case 7:	imprimeLCD("_______A_________",0);break;
			case 8:	imprimeLCD("________A________",0);break;
			case 9:	imprimeLCD("_________A_______",0);break;
			case 10:	imprimeLCD("__________A______",0);break;
			case 11:	imprimeLCD("___________A_____",0);break;
			case 12:	imprimeLCD("____________A____",0);break;
			case 13:	imprimeLCD("_____________A___",0);break;
			case 14:	imprimeLCD("______________A__",0);break;
			case 15:	imprimeLCD("_______________A_",0);break;
			case 16:	imprimeLCD("________________A",0);break;
			default:
				if(contador > 16){
					contador = 16;
					imprimeLCD("________________A",0);	
				}
				if(contador < 0){
					contador = 0;
					imprimeLCD("A_________________",0);
				}
		}
		
		copy_to_lcd(0);
		
		signals = osSignalWait(0x00,3000);
				
		if(signals.status == osEventTimeout){
			contador++;
		}
		
		if (signals.value.signals == RIGHT_SIGN){
			osTimerStart(timer_seg_id,250);
			GPIO_PinWrite(PUERTO_LED,LED_1,1);
			GPIO_PinWrite(PUERTO_LED,LED_2,0);
			contador++;
		}
		if (signals.value.signals == LEFT_SIGN){
			osTimerStart(timer_seg_id,250);
			GPIO_PinWrite(PUERTO_LED,LED_1,1);
			GPIO_PinWrite(PUERTO_LED,LED_2,0);
			contador--;
		}		
		
    osThreadYield ();                                           // suspend thread
  }
}

void  timer_seg_Callback(void const *argument){
				GPIO_PinWrite(PUERTO_LED,LED_1,0);
}
	


