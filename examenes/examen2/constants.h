//ctes leds
#define PUERTO_LED    		1
#define LED_1         		18
#define LED_2         		20
#define LED_3         		21
#define LED_4         		23

// CTE PARA LA VELOCIDAD DEL PERIFERICO SPI A 100 MHz		
#define SSP1_CLOCK_100MHz			20
// TAMA�O MAX BUFFER 
#define BUFFER_MAX            512
// CTES SPI
#define PUERTO_SPI						0
#define	PIN_RESET							8
#define PIN_A0								6
#define PIN_CS								18

//ctes JS
#define PUERTO_JS  				0
#define UP     						23					
#define DOWN							17    	
#define CENTER     				16		
#define LEFT			    		15		
#define RIGHT 						24


#define RIGHT_SIGN				0x01
#define CENTER_SIGN				0x02
#define LEFT_SIGN					0x04
#define UP_SIGN						0x08
#define DOWN_SIGN					0x10


#define MAX								18
