#include "constants.h"
#include "LPC17xx.h"
#include "GPIO_LPC17xx.h"
#include "PIN_LPC17xx.h"
#include "cmsis_os.h" 
#include "utils.h"
#include "LCD.h"

extern osThreadId tid_Thread;

extern osThreadId tid_Thread2;

void Initialize_peripherals(void){
	
		init_SPI();
		LCD_reset();
		
		GPIO_SetDir(PUERTO_LED,LED_1,GPIO_DIR_OUTPUT);
		PIN_Configure(PUERTO_LED,LED_1,PIN_FUNC_0,PIN_PINMODE_PULLDOWN,PIN_PINMODE_NORMAL);
	
		GPIO_SetDir(PUERTO_LED,LED_2,GPIO_DIR_OUTPUT);
		PIN_Configure(PUERTO_LED,LED_2,PIN_FUNC_0,PIN_PINMODE_PULLDOWN,PIN_PINMODE_NORMAL);
	
		GPIO_SetDir(PUERTO_LED,LED_3,GPIO_DIR_OUTPUT);
		PIN_Configure(PUERTO_LED,LED_3,PIN_FUNC_0,PIN_PINMODE_PULLDOWN,PIN_PINMODE_NORMAL);
	
		GPIO_SetDir(PUERTO_LED,LED_4,GPIO_DIR_OUTPUT);
		PIN_Configure(PUERTO_LED,LED_4,PIN_FUNC_0,PIN_PINMODE_PULLDOWN,PIN_PINMODE_NORMAL);
	
		//inicialization of the JS
		PIN_Configure(PUERTO_JS,CENTER,PIN_FUNC_0,PIN_PINMODE_PULLDOWN,PIN_PINMODE_NORMAL);
		PIN_Configure(PUERTO_JS,DOWN,PIN_FUNC_0,PIN_PINMODE_PULLDOWN,PIN_PINMODE_NORMAL);
		PIN_Configure(PUERTO_JS,UP,PIN_FUNC_0,PIN_PINMODE_PULLDOWN,PIN_PINMODE_NORMAL);
		PIN_Configure(PUERTO_JS,LEFT,PIN_FUNC_0,PIN_PINMODE_PULLDOWN,PIN_PINMODE_NORMAL);
		PIN_Configure(PUERTO_JS,RIGHT,PIN_FUNC_0,PIN_PINMODE_PULLDOWN,PIN_PINMODE_NORMAL);
		
		//clear the flags on the first place (to make sure)
		LPC_GPIOINT->IO0IntClr |= 1 << LEFT;
		LPC_GPIOINT->IO0IntClr |= 1 << DOWN;
		LPC_GPIOINT->IO0IntClr |= 1 << UP;
		LPC_GPIOINT->IO0IntClr |= 1 << CENTER;
		LPC_GPIOINT->IO0IntClr |= 1 << RIGHT;
		
		// enables the interruption on the fall
		LPC_GPIOINT->IO0IntEnF |= 1 << CENTER; 
		LPC_GPIOINT->IO0IntEnF |= 1 << DOWN; 
		LPC_GPIOINT->IO0IntEnF |= 1 << UP; 
		LPC_GPIOINT->IO0IntEnF |= 1 << LEFT; 
		LPC_GPIOINT->IO0IntEnF |= 1 << RIGHT; 
		
		NVIC_EnableIRQ(EINT3_IRQn);
}

void EINT3_IRQHandler(void)
{
  if (LPC_GPIOINT->IO0IntStatF & (1 << LEFT)){
		osDelay(200);
    osSignalSet(tid_Thread,LEFT_SIGN);
    osSignalSet(tid_Thread2,LEFT_SIGN);
    LPC_GPIOINT->IO0IntClr |= 1 << LEFT;
  }
  
  if(LPC_GPIOINT->IO0IntStatF & (1 << CENTER)){
		osDelay(200);
    osSignalSet(tid_Thread,CENTER_SIGN);
    LPC_GPIOINT->IO0IntClr |= 1 << CENTER;    
  }
  
  if(LPC_GPIOINT->IO0IntStatF & (1 << UP)){
		osDelay(200);
    osSignalSet(tid_Thread,UP_SIGN);
    LPC_GPIOINT->IO0IntClr |= 1 << UP;    
  }
  
  if(LPC_GPIOINT->IO0IntStatF & (1 << DOWN)){
		osDelay(200);
    osSignalSet(tid_Thread,DOWN_SIGN);
    LPC_GPIOINT->IO0IntClr |= 1 << DOWN;    
  } 
	
	if(LPC_GPIOINT->IO0IntStatF & (1 << RIGHT)){
		osDelay(200);
    osSignalSet(tid_Thread,RIGHT_SIGN);
		osSignalSet(tid_Thread2,LEFT_SIGN);
    LPC_GPIOINT->IO0IntClr |= 1 << RIGHT;    
  } 
}



