/*----------------------------------------------------------------------------
 * CMSIS-RTOS 'main' function template
 *---------------------------------------------------------------------------*/

#define osObjectsPublic                     // define objects in main module
#include "osObjects.h"                      // RTOS object definitions
#include "utils.h"


extern int Init_Thread (void);
/*
 * main: initialize and start the system
 */
int main (void) {
		
  osKernelInitialize ();                    // initialize CMSIS-RTOS
	Init_Thread();
	Initialize_peripherals();
  // initialize peripherals here

  // create 'thread' functions that start executing,
  // example: tid_name = osThreadCreate (osThread(name), NULL);

  osKernelStart ();                         // start thread execution 
}
