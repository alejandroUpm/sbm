#include "GPIO_LPC17xx.h"
#include "LPC17xx.h"

#define PORT_LEDS			1
#define PIN_LED1			18

#define RIT_CTRL_INTEN				((uint32_t) (1))

#define CLKPWR_PCONP_PCRIT		((uint32_t) (1<<16))

//por defecto el reloj del timer es 50 MHz
void RIT_IRQHandler(void){			
		static uint32_t valor = 0;
		
		LPC_RIT->RICTRL |= RIT_CTRL_INTEN;
		GPIO_PinWrite(PORT_LEDS,PIN_LED1,valor);
		valor = ~valor;
}

int main(){
	
	GPIO_SetDir(PORT_LEDS,PIN_LED1,GPIO_DIR_OUTPUT);
	
	LPC_SC->PCONP |= CLKPWR_PCONP_PCRIT;
	
	LPC_RIT->RICOMPVAL = 0x005F5E10;	// esta linea determina hasta que valor cuenta el timer		
	LPC_RIT->RIMASK = 0x00000000;			// esta es la mascara del timer
	LPC_RIT->RICTRL = 0x0C;						// registro de 32 bits, configurables solo los 4 primeros.
	LPC_RIT->RICOUNTER = 0x00000000;	// registro que almacena la cuenta del timer
	
	LPC_RIT->RICTRL |= (1<<1);				// ademas de la configuracion en la linea 28 se configura, este bit establece
																		// que la cuenta se va a cero cada vez que RIMASK y RICOMPVAL sean iguales
	
	NVIC_EnableIRQ(RIT_IRQn);

	while(1);
}






