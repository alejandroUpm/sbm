#include "GPIO_LPC17xx.h"
#include "LPC17xx.h"
#include "PIN_LPC17xx.h"

#define TIMER2_ENABLE				22
#define FREQ_TIMER0_100			12
#define PUERTO_TIMER2				0
#define PIN_TIMER2					6



int main ()
{
	//habilitacion del timer 2 con la frecuencia de 100 MHz
	LPC_SC->PCONP	|=1 << TIMER2_ENABLE;
	LPC_SC->PCLKSEL1 |= 1 << FREQ_TIMER0_100;
	//configuracion del timer al pin fisico 8
	GPIO_SetDir(PUERTO_TIMER2,PIN_TIMER2,GPIO_DIR_OUTPUT);
	PIN_Configure(PUERTO_TIMER2,PIN_TIMER2,PIN_FUNC_3,PIN_PINMODE_PULLUP,PIN_PINMODE_NORMAL);
	//configuracion del timer 2
	LPC_TIM2->TCR |= 0x1;	 
	LPC_TIM2->MR0 |= 0x0000C350;
	LPC_TIM2->MCR = 0x002;
	LPC_TIM2->EMR |= 0x031;
	// se habilita la interrupcion del timer2
	
	while(1);
}