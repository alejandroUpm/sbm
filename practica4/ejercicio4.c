#include "LPC17xx.h"
#include "PIN_LPC17xx.h"
#include "GPIO_LPC17xx.h"
#include "Driver_SPI.h"
#include "GPDMA_LPC17xx.h"
#include "LCD.h"



int main (void)
{
  init_SPI();
	LCD_reset();

  imprimeLCD("prueba valor1: ",0);
	escribirNumeroEntero(56,0);
	imprimeLCD(" m/s",0);
	copy_to_lcd(0);			
      
  imprimeLCD("prueba valor2: ",1);
	escribirNumeroDecimal(0.6545654564,1);
	copy_to_lcd(1);
	
	while(1){}
}
