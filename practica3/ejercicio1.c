#include "LPC17xx.h"
#include "PIN_LPC17xx.h"
#include "GPIO_LPC17xx.h"
#include "GPDMA_LPC17xx.h"
#include "Driver_SPI.h"



//	CTE PARA LA VELOCIDAD DEL PERIFERICO SPI A 100 MHz		
#define SSP1_CLOCK_100MHz			20

// CTES SPI
#define PUERTO_SPI						0
#define	PIN_RESET							8
#define PIN_A0								6
#define PIN_CS								18

// TAMA�O MAX BUFFER 
#define BUFFER_MAX            512

// importacion del driver y asignacion a una variable 
extern ARM_DRIVER_SPI Driver_SPI1;
ARM_DRIVER_SPI* SPIdrv = &Driver_SPI1;

const unsigned char buffer[BUFFER_MAX] = {0xFF,0x00,0xFF,0x00}; 

double variacion = 0.14;
// el  tiempo minimo que debe estar el reset a cero 
// para que se efectue debe ser de 1 us
void retraso (uint32_t microsegundos){
	
	for (int j = 0; j < variacion*microsegundos*1000 ; j++){}
  
}
 
//FUNCION DE CONFIGURACION E INICIALIZACION DEL PERIFERICO SPI
void init_SPI()
{
	//configuracion velocidad de periferico SPI
	LPC_SC->PCLKSEL0 |= 1 << SSP1_CLOCK_100MHz;
	// inicializa el driver SPI 
	SPIdrv->Initialize(NULL);
	// alimenta el periferico SPI 
	SPIdrv->PowerControl(ARM_POWER_FULL);
	// set up to master mode, Clock Polarity 1 y Clock Phase 1, Set the bit order from MSB to LSB,
	// Set the number of bits per SPI frame; range for n = 1..32 in our case  to 8
	SPIdrv->Control(ARM_SPI_MODE_MASTER |
									ARM_SPI_CPOL1_CPHA1	|
									ARM_SPI_MSB_LSB			|
									ARM_SPI_DATA_BITS(8),20000000);		// 20 MHz es la max frecuencia que puede soportar 
																										// el LCD
	
	// el master, osea el micro, es el que genera el cs de activacion del slave 
	GPIO_SetDir(PUERTO_SPI,PIN_CS,GPIO_DIR_OUTPUT);
	GPIO_PinWrite(PUERTO_SPI,PIN_CS,1);								
																										
	GPIO_SetDir(PUERTO_SPI,PIN_A0,GPIO_DIR_OUTPUT);
  GPIO_PinWrite(PUERTO_SPI,PIN_A0,1);
	
	GPIO_SetDir(PUERTO_SPI,PIN_RESET,GPIO_DIR_OUTPUT);
	GPIO_PinWrite(PUERTO_SPI,PIN_RESET,0);
	retraso(1);
	GPIO_PinWrite(PUERTO_SPI,PIN_RESET,1);
	retraso(1000);

}

void wr_data(unsigned char data){	
  GPIO_PinWrite(PUERTO_SPI,PIN_CS,0);
  GPIO_PinWrite(PUERTO_SPI,PIN_A0,1);
	SPIdrv->Send(&data,sizeof(data));
  GPIO_PinWrite(PUERTO_SPI,PIN_CS,1);
}

void wr_cmd(unsigned char cmd){
  GPIO_PinWrite(PUERTO_SPI,PIN_CS,0);
  GPIO_PinWrite(PUERTO_SPI,PIN_A0,0);
	SPIdrv->Send(&cmd,sizeof(cmd));
  GPIO_PinWrite(PUERTO_SPI,PIN_CS,1);
}

void LCD_reset(){
	wr_cmd(0xAE);
	wr_cmd(0xA2);
	wr_cmd(0xA0);
	wr_cmd(0xC8);
	wr_cmd(0x22);
	wr_cmd(0x2F);
	wr_cmd(0x40);
	wr_cmd(0xAF);
	wr_cmd(0x81);
	wr_cmd(0x17);
	wr_cmd(0xA4);
	wr_cmd(0xA6);
	
}


void copy_to_lcd(void)
{
    int i;
    wr_cmd(0x00);      // 4 bits de la parte baja de la direcci�n a 0
    wr_cmd(0x10);      // 4 bits de la parte alta de la direcci�n a 0
    wr_cmd(0xB0);      // P�gina 0
    
    for(i=0;i<128;i++){
        wr_data(buffer[i]);
        }
  
     
    wr_cmd(0x00);      // 4 bits de la parte baja de la direcci�n a 0
    wr_cmd(0x10);      // 4 bits de la parte alta de la direcci�n a 0
    wr_cmd(0xB1);      // P�gina 1
     
    for(i=128;i<256;i++){
        wr_data(buffer[i]);
        }
    
    wr_cmd(0x00);       
    wr_cmd(0x10);      
    wr_cmd(0xB2);      //P�gina 2
    for(i=256;i<384;i++){
        wr_data(buffer[i]);
        }
    
    wr_cmd(0x00);       
    wr_cmd(0x10);       
    wr_cmd(0xB3);      // Pagina 3
     
     
    for(i=384;i<512;i++){
        wr_data(buffer[i]);
        }
}

int main (void)
{
  init_SPI();
	LCD_reset();
	copy_to_lcd();
	while(1);
}









